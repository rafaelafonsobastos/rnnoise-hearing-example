//
// Created by Valentyn Smaha in 2024-01-19
//

#include "RnNoiseSuppressor.h"
#include <jni.h>
#include <android/log.h>

extern "C" {
#include "src/rnnoise.h"
}

RnNoiseSuppressor *rnNoiseSuppressor = new RnNoiseSuppressor();

void RnNoiseSuppressor::init() {
    rnNoiseSuppressor->st = rnnoise_create(NULL);
}

void RnNoiseSuppressor::enable(int enabled) {
    this->enabled = enabled != 0;
}

void RnNoiseSuppressor::process(float *data) {
    rnnoise_process_frame(rnNoiseSuppressor->st, data,
                          data);
}

void RnNoiseSuppressor::destroy() {
    rnnoise_destroy(rnNoiseSuppressor->st);
}

bool RnNoiseSuppressor::isEnabled() const {
    return enabled;
}


void RnNoiseSuppressor::reinitialize() {
    destroy();
    init();
}

extern "C" {

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_rnnoise_RnNoiseHelper_setEnabled(JNIEnv *env, jobject thiz,
                                                                         jboolean enabled) {
    rnNoiseSuppressor->enable(enabled ? 1 : 0);
}

JNIEXPORT void JNICALL
Java_com_example_rnnoise_RnNoiseHelper_process(JNIEnv *env, jobject thiz,
                                               jfloatArray buffer) {
    int len = env->GetArrayLength(buffer);
    jfloat *data = env->GetFloatArrayElements(buffer, NULL);

    __android_log_print(ANDROID_LOG_DEBUG, "RnNoiseHelper", "len of buffer: %d", len);
    rnNoiseSuppressor->process(data);
//    for (int i = 0; i < len; i += FRAME_SIZE) {
//        rnNoiseSuppressor->process(&data[i]);
//    }
    env->ReleaseFloatArrayElements(buffer, data, 0);
}

JNIEXPORT void JNICALL
Java_com_example_rnnoise_RnNoiseHelper_destroy(JNIEnv *env, jobject thiz) {
    rnNoiseSuppressor->destroy();
}

JNIEXPORT void JNICALL
Java_com_example_rnnoise_RnNoiseHelper_init(JNIEnv *env, jobject thiz) {
    rnNoiseSuppressor->init();
}

JNIEXPORT void JNICALL
Java_com_example_rnnoise_RnNoiseHelper_setEnabled(JNIEnv *env, jobject thiz, jboolean enabled) {
    rnNoiseSuppressor->enable(enabled ? 1 : 0);
}

JNIEXPORT void JNICALL
Java_com_example_rnnoise_RnNoiseHelper_reinitialize(JNIEnv *env, jobject thiz) {
    rnNoiseSuppressor->reinitialize();
}
}