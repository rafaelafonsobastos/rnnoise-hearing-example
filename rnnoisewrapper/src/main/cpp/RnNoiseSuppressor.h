//
// Created by Valentyn Smaha in 2024-01-19
//

#include "src/rnnoise.h"

#ifndef CHKIN_HEARING_RN_NOISESUPPRESSOR_H
#define CHKIN_HEARING_RN_NOISESUPPRESSOR_H
#define FRAME_SIZE 480


class RnNoiseSuppressor {
private:
    bool enabled;

public:
    DenoiseState *st;
    int sampleRate;
    int bufferSize;

    void init();

    void enable(int enabled);

    void process(float data[]);

    void destroy();

    bool isEnabled() const;

    void setSampleRate(int sampleRate);

    void setBufferSize(int bufferSize);

    void reinitialize();
};

extern RnNoiseSuppressor *rnNoiseSuppressor;

#endif //CHKIN_HEARING_RN_NOISESUPPRESSOR_H
