package com.example.rnnoise

import android.util.Log

class RnNoiseHelper{

    var isFilterEnabled = false

    companion object {
        init {
            System.loadLibrary("rnnoisewrapper")
        }
    }

    init {
        initialize()
        setEnabled(true)
    }

    private fun initialize() {
        Log.d("svcom", "initializing noise suppression RNNOISE in helper")
        init()
    }

    fun processSound(data: FloatArray, size: Int) {
        process(data)
    }

    external fun init()

    external fun setEnabled(enabled: Boolean)

    external fun process(buffer: FloatArray)

    external fun destroy()

    external fun reinitialize()
}