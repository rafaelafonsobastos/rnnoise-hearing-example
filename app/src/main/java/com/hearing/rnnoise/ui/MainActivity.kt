package com.hearing.rnnoise.ui

import android.Manifest.permission
import androidx.core.content.ContextCompat
import com.hearing.rnnoise.base.BaseActivity
import com.hearing.rnnoise.databinding.ActivityMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity<ActivityMainBinding>() {

    private val viewModel: MainViewModel by viewModel()

    override fun initBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
    }

    override fun setupUI() {
        observe()
        initControls()
    }

    private fun initControls() {
        binding.cbWithNoiseSuppression.setOnCheckedChangeListener { _, isChecked ->
            viewModel.setNoiseSuppressionEnabled(isChecked)
        }

        binding.btnStart.setOnClickListener {
            playWithPermission()
        }

        binding.btnStop.setOnClickListener {
            viewModel.stop()
        }
    }

    private fun playWithPermission() {
        if (hasMicPermission()){
            viewModel.play()
        } else {
            askMicPermission()
        }
    }

    private fun hasMicPermission(): Boolean {
        //check has RECORD_AUDIO permission
        return ContextCompat.checkSelfPermission(
            applicationContext,
            permission.RECORD_AUDIO
        ) == android.content.pm.PackageManager.PERMISSION_GRANTED
    }

    private fun askMicPermission() {
        //ask for permission
        requestPermissions(
            arrayOf(permission.RECORD_AUDIO),
            1
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == 1) {
            if (grantResults.isNotEmpty() && grantResults[0] == android.content.pm.PackageManager.PERMISSION_GRANTED) {
                playWithPermission()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun observe() {
        viewModel.isNoiseSuppressionEnabled.observe(this) {
            binding.cbWithNoiseSuppression.isChecked = it
        }

        viewModel.isPlaying.observe(this) {
            binding.btnStart.isEnabled = !it
            binding.btnStop.isEnabled = it
        }
    }
}