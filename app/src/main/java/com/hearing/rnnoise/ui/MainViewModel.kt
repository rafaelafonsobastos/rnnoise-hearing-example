package com.hearing.rnnoise.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.rnnoise.RnNoiseHelper
import com.hearing.rnnoise.base.BaseViewModel
import com.hearing.rnnoise.domain.audio.AudioEngineDefaultImpl

class MainViewModel(
    private val audioEngineDefaultImpl: AudioEngineDefaultImpl,
    private val rnNoiseHelper: RnNoiseHelper

): BaseViewModel() {

    private val _isNoiseSuppressionEnabled = MutableLiveData<Boolean>(rnNoiseHelper.isFilterEnabled)
    val isNoiseSuppressionEnabled: LiveData<Boolean> = _isNoiseSuppressionEnabled

    private val _isPlaying = MutableLiveData<Boolean>(false)
    val isPlaying: LiveData<Boolean> = _isPlaying

    init {
        audioEngineDefaultImpl.create()
    }


    fun setNoiseSuppressionEnabled(enabled: Boolean) {
        if (enabled != rnNoiseHelper.isFilterEnabled) {
            rnNoiseHelper.isFilterEnabled = enabled
            _isNoiseSuppressionEnabled.value = enabled
        }
    }

    fun play() {
        audioEngineDefaultImpl.setActive(true)
        _isPlaying.value = audioEngineDefaultImpl.isActive()
    }

    fun stop(){
        audioEngineDefaultImpl.setActive(false)
        _isPlaying.value = audioEngineDefaultImpl.isActive()
    }

}