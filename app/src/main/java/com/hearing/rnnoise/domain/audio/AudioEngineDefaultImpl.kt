package com.hearing.rnnoise.domain.audio

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.media.*
import android.media.AudioFormat.CHANNEL_OUT_FRONT_LEFT
import android.media.AudioFormat.CHANNEL_OUT_FRONT_RIGHT
import android.os.Build
import android.util.Log
import androidx.core.app.ActivityCompat
import com.hearing.rnnoise.domain.audio.AudioEngineImpl.Companion.CHANNEL_COUNT_MONO
import com.example.rnnoise.RnNoiseHelper
import com.hearing.rnnoise.data.PreferencesHelper
import com.hearing.rnnoise.data.waveprocessor.AudioEvent
import com.hearing.rnnoise.data.waveprocessor.WriteProcessor
import com.hearing.rnnoise.domain.audio.AudioEngineImpl.Companion.BUFFER_SIZE_480_SAMPLES

import com.hearing.rnnoise.util.playSafe
import com.hearing.rnnoise.util.startRecordingSafe
import com.hearing.rnnoise.util.stopSafe
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import java.io.BufferedInputStream
import java.io.File
import java.io.InputStream
import java.io.RandomAccessFile
import java.nio.ByteBuffer
import java.nio.ByteOrder
import kotlin.experimental.and
import kotlin.properties.Delegates

class AudioEngineDefaultImpl(
    private val context: Context,
    private val audioManager: AudioManager,
    private val rnNoiseHelper: RnNoiseHelper,
    private val preferencesHelper: PreferencesHelper
) : AudioEngineImpl {

    private var audioRecord: AudioRecord? = null

    private var audioTrack: AudioTrack? = null


    private var audioThread: AudioThread? = null

    private var audioRecordPreferredDevice: AudioDeviceInfo? = null

    private var audioTrackPreferredDevice: AudioDeviceInfo? = null

    private var isActive = false

    private var settingsChanged = true

//    private var settingSampleRate: Int by
//    Delegates.observable(preferencesHelper.sampleRate) { _, old, new ->
//        if (new != old) {
//            settingsChanged = true
//        }
//    }

    private val settingSampleRate = 48000

    private var settingChannelCount: Int by
    Delegates.observable(preferencesHelper.channelCount) { _, old, new ->
        if (new != old) {
            settingsChanged = true
        }
    }

    private var bufferSizeTrack = BUFFER_SIZE_480_SAMPLES

    //TODO: important to change the audio encoding to 16bit if using ShortArray buffer
//    private val audioEncoding = AudioFormat.ENCODING_PCM_16BIT

    //TODO: important to change the audio encoding to float if using FloatArray buffer
    private val audioEncoding = AudioFormat.ENCODING_PCM_16BIT

    @SuppressLint("MissingPermission")
    private fun initializeAudioTrackAndRecord(): Boolean {
        releaseAudioTrackAndRecord()

        bufferSizeTrack = BUFFER_SIZE_480_SAMPLES

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.RECORD_AUDIO
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return false
        }
        audioRecord = AudioRecord(
            MediaRecorder.AudioSource.MIC,
            settingSampleRate,
            if (settingChannelCount == AudioEngineImpl.CHANNEL_COUNT_MONO)
                AudioFormat.CHANNEL_IN_MONO
            else
                AudioFormat.CHANNEL_IN_STEREO,
            audioEncoding,
            BUFFER_SIZE_480_SAMPLES
        )

        val audioRecordInitialized = audioRecord?.state == AudioRecord.STATE_INITIALIZED

        val audioTrack =
            when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.O -> AudioTrack.Builder()
                    .setAudioFormat(
                        AudioFormat.Builder()
                            .setEncoding(audioEncoding)
                            .setSampleRate(settingSampleRate)
                            .setChannelMask(
//                                if (settingChannelCount == AudioEngineImpl.CHANNEL_COUNT_MONO)
//                                    CHANNEL_OUT_FRONT_LEFT
//                                else
//                                    CHANNEL_OUT_FRONT_LEFT or CHANNEL_OUT_FRONT_RIGHT
                                CHANNEL_COUNT_MONO
                            )
                            .build()
                    )
                    .setAudioAttributes(
                        AudioAttributes.Builder()
                            .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                            .setUsage(AudioAttributes.USAGE_MEDIA)
                            .setFlags(AudioAttributes.FLAG_LOW_LATENCY)
                            .build()
                    )
                    .setTransferMode(AudioTrack.MODE_STREAM)
                    .setSessionId(AudioManager.AUDIO_SESSION_ID_GENERATE)
                    .setBufferSizeInBytes(bufferSizeTrack)
                    .setPerformanceMode(AudioTrack.PERFORMANCE_MODE_LOW_LATENCY)
                    .build()

                else -> AudioTrack(
                    AudioAttributes.Builder()
                        .setUsage(AudioAttributes.USAGE_MEDIA)
                        .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                        .build(),
                    AudioFormat.Builder()
                        .setSampleRate(settingSampleRate)
                        .setChannelMask(
                            if (settingChannelCount == AudioEngineImpl.CHANNEL_COUNT_MONO)
                                CHANNEL_OUT_FRONT_LEFT
                            else
                                CHANNEL_OUT_FRONT_LEFT or CHANNEL_OUT_FRONT_RIGHT
                        )
                        .setEncoding(audioEncoding)
                        .build(),
                    bufferSizeTrack,
                    AudioTrack.MODE_STREAM,
                    AudioManager.AUDIO_SESSION_ID_GENERATE
                )
            }.also { this.audioTrack = it }
        val audioTrackInitialized = audioTrack.state == AudioTrack.STATE_INITIALIZED

        return audioRecordInitialized && audioTrackInitialized
    }

    private fun releaseAudioTrackAndRecord() {
        audioThread?.apply {
            finishRunning()
            join()
        }
        audioThread = null

        audioRecord?.release()
        audioRecord = null
        audioTrack?.release()
        audioTrack = null
    }

    private fun createAudioTrackAndRecordIfNeeded(): Boolean {
        val audioRecord = audioRecord
        val audioTrack = audioTrack

        return if (settingsChanged ||
            audioRecord == null || audioRecord.state != AudioRecord.STATE_INITIALIZED ||
            audioTrack == null || audioTrack.state != AudioTrack.STATE_INITIALIZED
        ) {
            settingsChanged = false
            initializeAudioTrackAndRecord()
        } else {
            true
        }
    }

    override fun create() = true

    override fun delete() = releaseAudioTrackAndRecord()

    @Throws(IllegalStateException::class)
    override fun setActive(active: Boolean) {
        isActive = active
        if (active) {
            if (!createAudioTrackAndRecordIfNeeded()) {
                throw IllegalStateException("Could not create AudioTrack and AudioRecord")
            }

            if (audioThread?.isAlive == true) {
                if (audioThread?.isRunning == true) {
                    audioThread?.finishRunning()
                }
                audioThread?.join(250)
            }

            val record = audioRecord
            val track = audioTrack


            if (record == null || track == null) {
                throw IllegalStateException(
                    "AudioRecord or AudioTrack is null: " +
                            "Record = ${record == null}; Track = ${track == null}}"
                )
            }
            audioThread = AudioThread(
                record,
                track,
                bufferSizeTrack,
                rnNoiseHelper,
                settingChannelCount != CHANNEL_COUNT_MONO,
                context
            )

        } else {
            audioThread?.finishRunning()
            audioThread?.join(250)
        }
    }


    override fun isActive() = isActive

    override fun setRecordingDevice(device: AudioDeviceInfo) {
        if (audioRecordPreferredDevice != device) {
            audioRecord?.preferredDevice = device
            audioRecordPreferredDevice = device

        }
    }

    override fun setPlaybackDevice(device: AudioDeviceInfo) {
        audioTrack?.preferredDevice = device
        audioTrackPreferredDevice = device
    }

    override fun setApiType(apiType: Int) = Unit

    override fun isAAudioSupported() = false

    override fun getUnderrunCount() =
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            try {
                audioTrack?.underrunCount ?: -1
            } catch (e: Exception) {
                -1
            }
        } else {
            -1
        }

    override fun getOverrunCount() = -1

    override fun setSampleRate(sampleRate: Int) {
//        settingSampleRate = sampleRate
    }

    override fun setChannelCount(channelCount: Int) {
        settingChannelCount = channelCount
    }


    override fun getAudioSessionId() = audioRecord?.audioSessionId ?: -1

    class AudioThread(
        private val record: AudioRecord,
        private val track: AudioTrack,
        private val bufferSizeInBytes: Int,
        private val rnNoiseSuppressor: RnNoiseHelper,
        private val isStereo: Boolean,
        private val context: Context
    ) : Thread("AudioEngineDefaultImplThread") {

        var isRunning = true
            private set


        private val recordingScope = CoroutineScope(Job() + Dispatchers.IO)

        init {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO)
            start()
        }

        private fun limitToShort(value: Float) = when {
            value > Short.MAX_VALUE -> Short.MAX_VALUE
            value < Short.MIN_VALUE -> Short.MIN_VALUE
            else -> value.toInt().toShort()
        }

        private fun limitToShort(value: Int) = when {
            value > Short.MAX_VALUE -> Short.MAX_VALUE
            value < Short.MIN_VALUE -> Short.MIN_VALUE
            else -> value.toShort()
        }

        override fun run() {
            try {
                record.startRecordingSafe()
                track.playSafe()


                val bufferSizeInShorts = bufferSizeInBytes / 2

                val bufferSizeInFloats = bufferSizeInBytes / 4

                val testFileName = getFileFromAssets(context, "LHONArecording.wav")
                Log.d("svcom", "testFileName: ${testFileName.absolutePath}")

                val inputStream: InputStream = BufferedInputStream(testFileName.inputStream())


                val buffer = ByteArray(bufferSizeInBytes / 2)

                var read: Int

                val recordingFile = File(context.cacheDir, "recording_processed1.wav")
                val audioFormat =
                    com.hearing.rnnoise.data.waveprocessor.AudioFormat(48000f, 16, 1, true, false)
                val randomAccessFile = RandomAccessFile(recordingFile, "rw")
                val writer = WriteProcessor(audioFormat, randomAccessFile)

                val event = AudioEvent(audioFormat)

                while (inputStream.read(buffer).also { read = it } != -1 && isRunning) {
                    val floatBuffer = byteArrayToFloatArray(buffer)

                    Log.d(
                        "svcom",
                        "isFilterEnabled - ${rnNoiseSuppressor.isFilterEnabled}, float buffer BEFORE: ${
                            floatBuffer.firstElementsToString(20)
                        }"
                    )
                    if (rnNoiseSuppressor.isFilterEnabled) {
                        rnNoiseSuppressor.processSound(floatBuffer, bufferSizeInFloats)
                    }
                    Log.d(
                        "svcom",
                        "isFilterEnabled - ${rnNoiseSuppressor.isFilterEnabled}, float buffer AFTER: ${
                            floatBuffer.firstElementsToString(20)
                        }"
                    )

                    val shorBuffer = floatArrayToShortArray(floatBuffer.copyOf())
                    track.write(shorBuffer, 0, shorBuffer.size, AudioTrack.WRITE_BLOCKING)

                    event.byteBuffer = shortArrayToByteArray(shorBuffer)
                    writer.process(event)

//                    track.write(byteBuffer, 0, read)
//                    track.write(floatBuffer, 0, floatBuffer.size, AudioTrack.WRITE_BLOCKING)
                }

                writer.processingFinished()
//                while (isRunning) {
//                    val read: Int
//
//                    //TODO: uncomment the corresponding buffer type and its following processing
////                    val buffer: ShortArray = ShortArray(bufferSizeInShorts)
//
//                    val floatBuffer: FloatArray = FloatArray(bufferSizeInFloats)
//
//                    try {
////                        read = record.read(buffer, 0, buffer.size)
//                        read = record.read(floatBuffer, 0, floatBuffer.size, AudioRecord.READ_BLOCKING)
//
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                        continue
//                    }
//
//                    if (rnNoiseSuppressor.isFilterEnabled) {
//
//
////                        val tempFloatBuffer = convertShortsToFloats(buffer)
//
////                        rnNoiseSuppressor.processSound(tempFloatBuffer, bufferSizeInShorts)
//                        rnNoiseSuppressor.processSound(floatBuffer, bufferSizeInShorts)
//
//                        val denoiseShortBuffer = convertFloatsToShorts(floatBuffer)
//
////                        for (i in buffer.indices) {
////                            buffer[i] = denoiseShortBuffer[i]
////                        }
//
//                    }
//
//
//                    //TODO: other processing will be done here before writing to the track
//
//
////                    track.write(buffer, 0, buffer.size)
//                    track.write(floatBuffer, 0, floatBuffer.size, AudioTrack.WRITE_BLOCKING)
//
//                }
            } catch (t: Throwable) {
                t.printStackTrace()
            } finally {
                frequencySweep = 0.0
                record.stopSafe()
                track.stopSafe()
                recordingScope.cancel()
            }
        }

        fun finishRunning() {
            isRunning = false
        }

        fun FloatArray.firstElementsToString(number: Int): String {
            val elements = this.take(number)
            return elements.joinToString(separator = ", ") { it.toString() }
        }

        fun floatArrayToByteArray(floatArray: FloatArray): ByteArray {
            val byteArray = ByteArray(floatArray.size * 2) // Assuming 16-bit encoding (2 bytes per sample)
            val byteBuffer = ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN)

            for (f in floatArray) {
                val sample = (f * Short.MAX_VALUE).toInt().coerceIn(Short.MIN_VALUE.toInt(), Short.MAX_VALUE.toInt())
                byteBuffer.putShort(sample.toShort())
            }

            return byteArray
        }

        fun shortArrayToByteArray(shortArray: ShortArray): ByteArray {
            val byteArray = ByteArray(shortArray.size * 2) // Each short takes 2 bytes
            val byteBuffer = ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN)

            for (s in shortArray) {
                byteBuffer.putShort(s)
            }

            return byteArray
        }

        fun floatArrayToShortArray(floatArray: FloatArray): ShortArray {
            val shortArray = ShortArray(floatArray.size)
            for (i in floatArray.indices) {
                val sample = (floatArray[i] * Short.MAX_VALUE).toInt()
                    .coerceIn(Short.MIN_VALUE.toInt(), Short.MAX_VALUE.toInt())
                shortArray[i] = sample.toShort()
            }
            return shortArray
        }

        fun byteArrayToFloatArray(byteArray: ByteArray): FloatArray {
            if (byteArray.size % 2 != 0) {
                throw IllegalArgumentException("ByteArray length must be a multiple of 2")
            }

            val floatArraySize = byteArray.size / 2
            val floatArray = FloatArray(floatArraySize)

            val buffer = ByteBuffer.wrap(byteArray).order(ByteOrder.LITTLE_ENDIAN)
            for (i in 0 until floatArraySize) {
                val sample = buffer.short.toInt()
                // Normalize to range [-1.0, 1.0]
                floatArray[i] = sample.toFloat() / Short.MAX_VALUE
            }

            return floatArray
        }

//        fun floatArrayToByteArray(floatArray: FloatArray): ByteArray {
//            val byteBuffer = ByteBuffer.allocate(floatArray.size * 2).order(ByteOrder.LITTLE_ENDIAN)
//            for (f in floatArray) {
//                // Scale float value to short range
//                val sample = (f * Short.MAX_VALUE).toInt().toShort()
//                byteBuffer.putShort(sample)
//            }
//            return byteBuffer.array()
//        }
//TEST
//        fun byteArrayToFloatArray(byteArray: ByteArray): FloatArray {
//            if (byteArray.size % 4 != 0) {
//                throw IllegalArgumentException("ByteArray length must be a multiple of 4")
//            }
//
//            val floatArraySize = byteArray.size / 4
//            val floatArray = FloatArray(floatArraySize)
//
//            var byteIndex = 0
//            for (i in 0 until floatArraySize) {
//                val intBits =
//                    (byteArray[byteIndex++].toInt() and 0xFF) or
//                            (byteArray[byteIndex++].toInt() and 0xFF shl 8) or
//                            (byteArray[byteIndex++].toInt() and 0xFF shl 16) or
//                            (byteArray[byteIndex++].toInt() and 0xFF shl 24)
//                floatArray[i] = Float.fromBits(intBits)
//            }
//
//            return floatArray
//        }
//
//        fun floatArrayToByteArray(floatArray: FloatArray): ByteArray {
//            val byteList = mutableListOf<Byte>()
//
//            for (f in floatArray) {
//                val intBits = f.toBits()
//                byteList.add((intBits and 0xFF).toByte())
//                byteList.add((intBits shr 8 and 0xFF).toByte())
//                byteList.add((intBits shr 16 and 0xFF).toByte())
//                byteList.add((intBits shr 24 and 0xFF).toByte())
//            }
//
//            return byteList.toByteArray()
//        }

        //conversion of short array to float array based of the primitive size
        //it can be wrong, because the sound after conversion and the reversed conversion is broken
        private fun convertShortsToFloats(shortArray: ShortArray): FloatArray {
            // Ensure the input array has a non-negative length
            require(shortArray.isNotEmpty()) { "Input ShortArray must not be empty." }

            // If the input array length is odd, add a zero-filled element
            val adjustedSize =
                if (shortArray.size % 2 == 0) shortArray.size else shortArray.size + 1
            val floatArray = FloatArray(adjustedSize / 2)

            for (i in shortArray.indices step 2) {
                val combinedInt =
                    (shortArray[i].toInt() shl 16) or (shortArray[i + 1].toInt() and 0xFFFF)
                floatArray[i / 2] = Float.fromBits(combinedInt)
            }

            return floatArray
        }

        //possible correct conversion of float array to short array based of the primitive size
        private fun convertFloatsToShorts(floatArray: FloatArray): ShortArray {
            // Ensure the input array has an even length (since we're combining pairs)
            require(floatArray.size % 2 == 0) { "Input FloatArray must have an even length." }

            val shortArray = ShortArray(floatArray.size * 2)

            for (i in floatArray.indices) {
                val floatBits = floatArray[i].toBits()
                shortArray[i * 2] = (floatBits and 0xFFFF).toShort() // Lower 16 bits
                shortArray[i * 2 + 1] = (floatBits ushr 16).toShort() // Upper 16 bits
            }

            return shortArray
        }

        private fun getFileFromAssets(context: Context, fileName: String): File =
            File(context.cacheDir, fileName)
                .also {
                    if (!it.exists()) {
                        it.outputStream().use { cache ->
                            context.assets.open(fileName).use { inputStream ->
                                inputStream.copyTo(cache)
                            }
                        }
                    }
                }

//        private fun getFileFromAssets(context: Context, fileName: String): File = File(context.cacheDir, fileName)

        companion object {
            var frequencySweep = 0.0
        }
    }
}