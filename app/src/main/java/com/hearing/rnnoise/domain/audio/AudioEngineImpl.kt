package com.hearing.rnnoise.domain.audio

import android.media.AudioDeviceInfo

interface AudioEngineImpl {

    fun create(): Boolean

    fun delete()

    fun setActive(active: Boolean)

    fun isActive(): Boolean

    fun setRecordingDevice(device: AudioDeviceInfo)

    fun setPlaybackDevice(device: AudioDeviceInfo)

    fun setApiType(apiType: Int)

    fun isAAudioSupported(): Boolean

    fun getUnderrunCount(): Int

    fun getOverrunCount(): Int

    fun setSampleRate(sampleRate: Int)

    fun setChannelCount(channelCount: Int)

    fun getAudioSessionId(): Int


    enum class NoiseSuppressorEngine(type: Int) {
        SPEEX(0), ANDROID(1), RNNOISE(2)
    }

    companion object {
        // these constants should correspond to JniBridge.cpp constants
        const val API_AAUDIO = 0
        const val API_OPENSL_ES = 1
        const val API_DEFAULT = 2

        const val SAMPLE_RATE_48000 = 48000

        const val CHANNEL_COUNT_MONO = 1
        const val CHANNEL_COUNT_STEREO = 2

        const val BUFFER_SIZE_480_SAMPLES = 1920 // 480 samples * 4 bytes per sample
    }
}