package com.hearing.rnnoise.domain.audio

import com.hearing.rnnoise.data.PreferencesHelper

class AudioEngineHelper(
    private val preferencesHelper: PreferencesHelper,
    private val audioEngineDefaultImpl: AudioEngineDefaultImpl
) {

    // current implementation is changed with engine's API type
    private var currentImpl = IMPLEMENTATION_DEFAULT


    private val implementations = arrayOf(
        audioEngineDefaultImpl
    )

    var channelCount =
        preferencesHelper.channelCount
        set(value) = value.let {
            field = it
            preferencesHelper.channelCount = it
            safeChangeSetting {
                setEngineChannelCount(it)
            }
        }

    fun create() =
        implementations[IMPLEMENTATION_NATIVE].create() &&
                implementations[IMPLEMENTATION_DEFAULT].create()

    fun delete() {
        implementations[IMPLEMENTATION_NATIVE].delete()
        implementations[IMPLEMENTATION_DEFAULT].delete()
    }


    fun setActive(active: Boolean) =
        implementations[currentImpl].setActive(
            active
        )

    fun isActive() = implementations[currentImpl].isActive()

    fun isAAudioSupported() = implementations.first().isAAudioSupported()


    fun setEngineChannelCount(channelCount: Int) {
        implementations[currentImpl].setChannelCount(channelCount)
    }

    private inline fun safeChangeSetting(changes: AudioEngineHelper.() -> Unit) {
        val wasActive = isActive()
        if (wasActive) {
            setActive(false)
        }
        @Suppress("UNUSED_EXPRESSION")
        changes()
        if (wasActive) {
            setActive(true)
        }
    }

    companion object {
        private const val IMPLEMENTATION_NATIVE = 0
        private const val IMPLEMENTATION_DEFAULT = 1

        private fun apiTypeToImplementation(apiType: Int) =
            if (apiType == AudioEngineImpl.API_DEFAULT) {
                IMPLEMENTATION_DEFAULT
            } else {
                IMPLEMENTATION_NATIVE
            }
    }
}