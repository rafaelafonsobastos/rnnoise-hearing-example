package com.hearing.rnnoise.data

import android.content.Context
import android.preference.PreferenceManager
import androidx.core.content.edit
import com.hearing.rnnoise.domain.audio.AudioEngineImpl

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class PreferencesHelper(
    private val context: Context
) {

    private val preferences = PreferenceManager.getDefaultSharedPreferences(context)

    var sampleRate: Int
        get() = preferences.getInt(KEY_SAMPLE_RATE, AudioEngineImpl.SAMPLE_RATE_48000)
        set(value) = preferences.edit { putInt(KEY_SAMPLE_RATE, value) }


    var channelCount: Int
        get() = preferences.getInt(KEY_CHANNEL_COUNT, AudioEngineImpl.CHANNEL_COUNT_STEREO)
        set(value) = preferences.edit { putInt(KEY_CHANNEL_COUNT, value) }


    companion object {
        const val KEY_SAMPLE_RATE = "KEY_SAMPLE_RATE"
        const val KEY_CHANNEL_COUNT = "KEY_CHANNEL_COUNT"
    }
}