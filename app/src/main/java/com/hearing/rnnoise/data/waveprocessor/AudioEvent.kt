package com.hearing.rnnoise.data.waveprocessor


/**
 * An audio event flows through the processing pipeline. The object is reused for performance reasons.
 * The arrays with audio information are also reused, so watch out when using the buffer getter and setters.
 *
 * @author Joren Six
 */
class AudioEvent(
    /**
     * The format specifies a particular arrangement of data in a sound stream.
     */
    private val format: AudioFormat
) {
    /**
     * The audio block in floats
     * @return The float representation of the audio block.
     */
    /**
     * Set a new audio block.
     * @param floatBuffer The audio block that is passed to the next processor.
     */
    /**
     * The audio data encoded in floats from -1.0 to 1.0.
     */
    private var floatBuffer: FloatArray? = null

    /**
     * The audio data encoded in bytes according to format.
     */
    var byteBuffer: ByteArray? = null

    /**
     * The overlap in samples between blocks of audio
     * @return The overlap in samples between blocks of audio
     */
    /**
     * Change the default overlap (in samples)
     * @param newOverlap The new overlap between audio blocks.
     */
    /**
     * The overlap in samples.
     */
    private var overlap: Int = 0
    /**
     * The length of the stream, expressed in sample frames rather than byte
     * @return  The length of the stream, expressed in sample frames rather than bytes
     */
    /**
     * The length of the stream, expressed in sample frames rather than bytes
     */
    val frameLength: Long = 0

    /**
     * The number of bytes processed before this event. It can be used to calculate the time stamp for when this event started.
     */
    private var bytesProcessed: Long = 0
    private var bytesProcessing = 0

    /**
     * The audio sample rate in Hz
     * @return  The audio sample rate in Hz
     */
    val sampleRate: Float
        get() = format.sampleRate

    /**
     * The size of the buffer in samples.
     * @return The size of the buffer in samples.
     */
    fun getBufferSize(): Int {
        return floatBuffer?.size ?: 0
    }

    /**
     * Change the number of bytes processed
     *
     * @param bytesProcessed The number of bytes processed.
     */
    fun setBytesProcessed(bytesProcessed: Long) {
        this.bytesProcessed = bytesProcessed
    }

    /**
     * Calculates and returns the time stamp at the beginning of this audio event.
     * @return The time stamp at the beginning of the event in seconds.
     */
    fun getTimeStamp(): Double {
        return (bytesProcessed / format.frameSize / format.sampleRate).toDouble()
    }

    /**
     * The timestamp at the end of the buffer (in seconds)
     * @return The timestamp at the end of the buffer (in seconds)
     */
    fun getEndTimeStamp(): Double {
        return ((bytesProcessed + bytesProcessing) / format.frameSize / format.sampleRate).toDouble()
    }

    /**
     * The number of samples processed.
     * @return The number of samples processed.
     */
    fun getSamplesProcessed(): Long {
        return bytesProcessed / format.frameSize
    }

    /**
     * Calculate the progress in percentage of the total number of frames.
     *
     * @return a percentage of processed frames or a negative number if the
     * number of frames is not known beforehand.
     */
    fun getProgress(): Double {
        return bytesProcessed / format.frameSize / frameLength.toDouble()
    }

    companion object {
        /**
         * Calculates and returns the root mean square of the signal. Please
         * cache the result since it is calculated every time.
         * @param floatBuffer The audio buffer to calculate the RMS for.
         * @return The [RMS](http://en.wikipedia.org/wiki/Root_mean_square) of
         * the signal present in the current buffer.
         */
        fun calculateRMS(floatBuffer: FloatArray): Double {
            var rms = 0.0
            for (i in floatBuffer.indices) {
                rms += (floatBuffer[i] * floatBuffer[i]).toDouble()
            }
            rms = rms / java.lang.Double.valueOf(floatBuffer.size.toDouble())
            rms = Math.sqrt(rms)
            return rms
        }

        /**
         * Returns the dBSPL for a buffer.
         *
         * @param buffer
         * The buffer with audio information.
         * @return The dBSPL level for the buffer.
         */
        private fun soundPressureLevel(buffer: FloatArray): Double {
            val rms = calculateRMS(buffer)
            return linearToDecibel(rms)
        }

        /**
         * Converts a linear to a dB value.
         *
         * @param value
         * The value to convert.
         * @return The converted value.
         */
        private fun linearToDecibel(value: Double): Double {
            return 20.0 * Math.log10(value)
        }
    }
}