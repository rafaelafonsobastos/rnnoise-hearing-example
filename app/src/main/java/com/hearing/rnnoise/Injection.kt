package com.hearing.rnnoise

import android.app.ActivityManager
import android.content.Context
import android.media.AudioManager
import com.example.rnnoise.RnNoiseHelper
import com.hearing.rnnoise.data.PreferencesHelper
import com.hearing.rnnoise.domain.audio.AudioEngineDefaultImpl
import com.hearing.rnnoise.domain.audio.AudioEngineHelper
import com.hearing.rnnoise.ui.MainViewModel
import com.hearing.rnnoise.util.CoroutineScopeQualifier
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

object Injection {
    val koinModule = module {
        single(named(CoroutineScopeQualifier.GLOBAL)) {
            CoroutineScope(SupervisorJob() + Dispatchers.Main)
        }

        single {
            get<Context>().getSystemService(Context.AUDIO_SERVICE) as AudioManager
        }

        single(createdAtStart = true) { PreferencesHelper(get()) }
        single(createdAtStart = true) { RnNoiseHelper() }

        single(createdAtStart = true) {
            AudioEngineDefaultImpl(get(), get(), get(), get())
        }
        single(createdAtStart = true) {
            AudioEngineHelper(
                get(),
                get(),
            )
        }

        single {
            get<Context>().getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        }




        viewModel {
            MainViewModel(
                get(), get()
            )
        }
    }

}
