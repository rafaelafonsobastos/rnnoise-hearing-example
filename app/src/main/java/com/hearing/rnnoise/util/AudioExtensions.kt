package com.hearing.rnnoise.util

import android.media.AudioManager
import android.media.AudioRecord
import android.media.AudioTrack

object AudioManagerExtension {
    const val STREAM_BLUETOOTH_SCO = 6
}

//fun AudioManager.getAudioTrackBufferSize(multiplier: Int = 1) = (
//    getProperty(AudioManager.PROPERTY_OUTPUT_FRAMES_PER_BUFFER)
//        ?.toIntOrNull()
//        ?.takeUnless { it == 0 || it % 8 != 0}
//        ?: 256
//    ) * multiplier

fun AudioManager.getAudioTrackBufferSize(multiplier: Int = 1) = 1920

fun AudioManager.getNativeSampleRate() =
    getProperty(AudioManager.PROPERTY_OUTPUT_SAMPLE_RATE)
        ?.toIntOrNull()
        ?.takeUnless { it == 0 }
        ?: 48000

fun AudioTrack.playSafe() {
    if (playState != AudioTrack.PLAYSTATE_PLAYING) {
        play()
    }
}

fun AudioTrack.stopSafe() {
    if (playState == AudioTrack.PLAYSTATE_PLAYING) {
        stop()
    }
}

fun AudioRecord.startRecordingSafe() {
    if (recordingState != AudioRecord.RECORDSTATE_RECORDING) {
        startRecording()
    }
}

fun AudioRecord.stopSafe() {
    if (recordingState == AudioRecord.RECORDSTATE_RECORDING) {
        stop()
    }
}