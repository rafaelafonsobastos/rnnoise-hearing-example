package com.hearing.rnnoise.base

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.core.component.KoinComponent
import kotlin.coroutines.CoroutineContext

abstract class BaseActivity<B: ViewBinding> : AppCompatActivity(), CoroutineScope, KoinComponent {

    protected open fun setupUI() = Unit

    private val job = SupervisorJob()


    lateinit var binding: B

    abstract fun initBinding()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        setContentView(binding.root)
        setupUI()
    }
}