package com.hearing.rnnoise.base

import android.content.Context
import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.hearing.rnnoise.data.PreferencesHelper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel : ViewModel(), KoinComponent, CoroutineScope {

    protected val appContext: Context by inject()

    protected val preferencesHelper: PreferencesHelper by inject()


    private val job = SupervisorJob()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job


    @CallSuper
    override fun onCleared() {
        job.cancel()
    }
}