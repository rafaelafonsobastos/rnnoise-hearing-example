package com.hearing.rnnoise

import android.app.Application
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import org.koin.android.BuildConfig
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin
import kotlin.coroutines.CoroutineContext

class App : Application(), CoroutineScope, KoinComponent {

    override val coroutineContext: CoroutineContext =
        SupervisorJob() + Dispatchers.Main.immediate

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@App)
            modules(Injection.koinModule)
            if (BuildConfig.DEBUG) {
                androidLogger()
            }
        }
    }

    companion object {
        init {
            System.loadLibrary("audio-engine")
        }
    }
}