# For more information about using CMake with Android Studio, read the
# documentation: https://d.android.com/studio/projects/add-native-code.html

# Sets the minimum version of CMake required to build the native library.

cmake_minimum_required(VERSION 3.4.1)

add_definitions(-DUSE_ALLOCA -DFIXED_POINT -DUSE_KISS_FFT -DEXPORT= -UHAVE_CONFIG_H)

# Creates and names a library, sets it as either STATIC
# or SHARED, and provides the relative paths to its source code.
# You can define multiple libraries, and CMake builds them for you.
# Gradle automatically packages shared libraries with your APK.

add_library( # Sets the name of the library.
        audio-engine

        # Sets the library as a shared library.
        SHARED

        # Provides a relative path to your source file(s).
#        rnnoise/include/rnnoise.h
#        rnnoise/src/_kiss_fft_guts.h
#        rnnoise/src/celt_lpc.c
#        rnnoise/src/celt_lpc.h
#        rnnoise/src/common.h
#        rnnoise/src/denoise.c
#        rnnoise/src/kiss_fft.c
#        rnnoise/src/kiss_fft.h
#        rnnoise/src/opus_types.h
#        rnnoise/src/mathops.h
#        rnnoise/src/mathops.c
#        rnnoise/src/pitch.c
#        rnnoise/src/pitch.h
#        rnnoise/src/rnn.c
#        rnnoise/src/rnn.h
#        rnnoise/src/rnn_data.c
#        rnnoise/src/rnn_data.h
#        rnnoise/src/rnn_reader.c
#        rnnoise/src/tansig_table.h
#        rnnoise/src/os_support.h
#        rnnoise/RnNoiseSuppressor.cpp
        noise_suppression/libspeexdsp/buffer.c
        noise_suppression/libspeexdsp/fftwrap.c
        noise_suppression/libspeexdsp/filterbank.c
        noise_suppression/libspeexdsp/jitter.c
        noise_suppression/libspeexdsp/kiss_fft.c
        noise_suppression/libspeexdsp/kiss_fftr.c
        noise_suppression/libspeexdsp/mdf.c
        noise_suppression/libspeexdsp/preprocess.c
        noise_suppression/libspeexdsp/resample.c
        noise_suppression/libspeexdsp/scal.c
        noise_suppression/libspeexdsp/smallft.c
        noise_suppression/libspeexdsp/speex_process.c
        noise_suppression/NoiseSuppressor.cpp
        equalizer/iir/XYData.cpp
        equalizer/iir/IIRControls.cpp
        equalizer/iir/IIREqualizer.cpp
        equalizer/Equalizer.cpp
        analyzer/data/ObservableShortArray.cpp
        analyzer/filters/HammingWindow.cpp
        analyzer/filters/LowPassFilter.cpp
        analyzer/fft/FFT.cpp
        analyzer/Analyzer.cpp
        bandpass/mkfilter/complex.cpp
        bandpass/mkfilter/mkfilter.cpp
        bandpass/BandpassFilter.cpp
        FullDuplexStream.cpp
        LiveEffectEngine.cpp
        JniBridge.cpp)


# Searches for a specified prebuilt library and stores the path as a
# variable. Because CMake includes system libraries in the search path by
# default, you only need to specify the name of the public NDK library
# you want to add. CMake verifies that the library exists before
# completing its build.

find_library( # Sets the name of the path variable.
        log-lib

        # Specifies the name of the NDK library that
        # you want CMake to locate.
        log)

# Specifies libraries CMake should link to your target library. You
# can link multiple libraries, such as libraries you define in this
# build script, prebuilt third-party libraries, or system libraries.

target_link_libraries( # Specifies the target library.
        audio-engine

        oboe

        # Links the target library to the log library
        # included in the NDK.
        ${log-lib})

# Set the path to the Oboe directory.
set(OBOE_DIR ../../../../oboe)

# Add the Oboe library as a subdirectory in your project.
add_subdirectory(${OBOE_DIR} ./oboe)

# Specify the path to the Oboe header files.
include_directories(${OBOE_DIR}/include)