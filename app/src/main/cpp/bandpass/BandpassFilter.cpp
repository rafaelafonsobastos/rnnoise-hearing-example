//
// Created by Roman Samoilenko on 2019-06-25.
//

#include "BandpassFilter.h"

BandpassFilter *bandpassFilter = new BandpassFilter();

static short limitToShort(double value) {
    if (value > SHRT_MAX) {
        return SHRT_MAX;
    } else if (value < SHRT_MIN) {
        return SHRT_MIN;
    } else {
        return static_cast<short>(value);
    }
}

static std::string toString(const double value, const int n = 12) {
    std::ostringstream out;
    out.precision(n);
    out << std::fixed << value;
    return out.str();
}

bool BandpassFilter::isEnabled() const {
    return enabled;
}

void BandpassFilter::setEnabled(bool enabled) {
    BandpassFilter::enabled = enabled;
}

void BandpassFilter::generateCoefficients() {
    double coefficientsMin = frequencyMin / (double) sampleRate;
    double coefficientsMax = frequencyMax / (double) sampleRate;

    std::vector<std::string> args = {
            "",
            "-Bu",
            "-Bp",
            "-o",
            "3",
            "-a",
            toString(coefficientsMin),
            toString(coefficientsMax),
            "-l"
    };

    std::vector<char *> argv;
    for (std::string &s: args) {
        argv.push_back(&s[0]);
    }
    argv.push_back(nullptr);

    double *result = mkfilter_process(argv.data());
    gain = result[0];
    coefficients[0] = result[1];
    coefficients[1] = result[2];
    coefficients[2] = result[3];
    coefficients[3] = result[4];
    coefficients[4] = result[5];
    coefficients[5] = result[6];
    delete[] result;

    for (int i = 0; i < (NZEROS + 1); i++) {
        xv[i] = 0.0F;
        yv[i] = 0.0F;
    }

//    __android_log_print(
//            ANDROID_LOG_ERROR,
//            "TAGGG",
//            "generate coefficients from sampleRate = %d, "
//            "min = %d, max = %d\nGain = %.10e\n"
//            "Coefficients:\n%.10e\n%.10e\n%.10e\n%.10e\n%.10e\n%.10e\n",
//            sampleRate, frequencyMin, frequencyMax, gain,
//            coefficients[0], coefficients[1], coefficients[2],
//            coefficients[3], coefficients[4], coefficients[5]
//    );
}

void BandpassFilter::setParameters(int sampleRate, int frequencyMin, int frequencyMax) {
    this->sampleRate = sampleRate;
    this->frequencyMin = frequencyMin < 16 ? 16 : frequencyMin;
    this->frequencyMax = frequencyMax > (sampleRate / 2 - 16)
            ? (sampleRate / 2 - 16) : frequencyMax;
    generateCoefficients();
}

void BandpassFilter::setSampleRate(int sampleRate) {
    this->sampleRate = sampleRate;
    generateCoefficients();
}

void BandpassFilter::setFrequencyRange(int frequencyMin, int frequencyMax) {
    this->frequencyMin = frequencyMin < 16 ? 16 : frequencyMin;
    this->frequencyMax = frequencyMax > (sampleRate / 2 - 16)
            ? (sampleRate / 2 - 16) : frequencyMax;
    generateCoefficients();

    int delta = this->frequencyMax - this->frequencyMin;
    if (delta < 64) {
        this->frequencyMax += 48 - delta;
    }
}

void BandpassFilter::process(short *data, int size) {
    for (int i = 0; i < size; i++) {
        xv[0] = xv[1];
        xv[1] = xv[2];
        xv[2] = xv[3];
        xv[3] = xv[4];
        xv[4] = xv[5];
        xv[5] = xv[6];
        xv[6] = data[i] / gain;
        yv[0] = yv[1];
        yv[1] = yv[2];
        yv[2] = yv[3];
        yv[3] = yv[4];
        yv[4] = yv[5];
        yv[5] = yv[6];
        yv[6] = (xv[6] - xv[0]) + 3 * (xv[2] - xv[4])
                + (coefficients[0] * yv[0]) + (coefficients[1] * yv[1])
                + (coefficients[2] * yv[2]) + (coefficients[3] * yv[3])
                + (coefficients[4] * yv[4]) + (coefficients[5] * yv[5]);
        data[i] = limitToShort(yv[6]);
    }
}

extern "C" {

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_bandpass_BandpassFilterHelper_create(JNIEnv *env,
                                                                             jobject,
                                                                             jboolean enabled,
                                                                             jint sampleRate,
                                                                             jint frequencyMin,
                                                                             jint frequencyMax) {
    bandpassFilter->setEnabled(enabled);
    bandpassFilter->setParameters(sampleRate, frequencyMin, frequencyMax);
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_bandpass_BandpassFilterHelper_setEnabled(JNIEnv *env,
                                                                                 jobject,
                                                                                 jboolean enabled) {
    bandpassFilter->setEnabled(enabled);
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_bandpass_BandpassFilterHelper_setSampleRate(
        JNIEnv *env,
        jobject,
        jint sampleRate
) {
    bandpassFilter->setSampleRate(sampleRate);
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_bandpass_BandpassFilterHelper_setFrequencyRange(
        JNIEnv *env,
        jobject,
        jint frequencyMin,
        jint frequencyMax
) {
    bandpassFilter->setFrequencyRange(frequencyMin, frequencyMax);
}

JNIEXPORT jboolean JNICALL
Java_com_chkin_hearing_data_local_audio_bandpass_BandpassFilterHelper_process(JNIEnv *env,
                                                                              jobject,
                                                                              jshortArray data,
                                                                              jint numberOfFrames) {
    if (!bandpassFilter->isEnabled()) {
        return JNI_FALSE;
    }

    short *audio = env->GetShortArrayElements(data, nullptr);
    bandpassFilter->process(audio, numberOfFrames);
    env->ReleaseShortArrayElements(data, audio, 0);
    return JNI_TRUE;
}
}