//
// Created by Roman Samoilenko on 2019-07-02.
//

#ifndef CHKIN_HEARING_BANDPASSFILTER_H
#define CHKIN_HEARING_BANDPASSFILTER_H

#include <jni.h>
#include <climits>
#include <string>
#include <sstream>
#include <vector>
#include <android/log.h>
#include "mkfilter/mkfilter.h"

class BandpassFilter {
public:
    bool isEnabled() const;

    void setEnabled(bool enabled);

    void setParameters(int sampleRate, int frequencyMin, int frequencyMax);

    void setSampleRate(int sampleRate);

    void setFrequencyRange(int frequencyMin, int frequencyMax);

    void process(short *data, int size);

private:
    static constexpr int NZEROS = 6;
    static constexpr int NPOLES = 6;

    double xv[NZEROS + 1];
    double yv[NPOLES + 1];
    double gain = 6.358933049e+02;
    double coefficients[6] = {
            -0.6041096995, 3.9187189277, -10.6181883660, 15.3828650250, -12.5654843040, 5.4861983461
    };

    bool enabled;
    int sampleRate;
    int frequencyMin;
    int frequencyMax;

    void generateCoefficients();
};

extern BandpassFilter *bandpassFilter;

#endif //CHKIN_HEARING_BANDPASSFILTER_H
