//
// Created by Roman Samoilenko on 2019-11-26.
//

#include "NoiseSuppressor.h"
#include <jni.h>
#include "../../../../../oboe/src/common/OboeDebug.h"

extern "C" {
#include "libspeexdsp/speex_process.h"
}

NoiseSuppressor *noiseSuppressor = new NoiseSuppressor();

void NoiseSuppressor::init() {
    speex_init(this->bufferSize, this->sampleRate, this->sensitivity);
}

void NoiseSuppressor::enable(int enabled) {
    this->enabled = enabled != 0;
    speex_enable(enabled);
}

int NoiseSuppressor::process(short *data) {
    return speex_process(data);
}

void NoiseSuppressor::destroy() {
    speex_destroy();
}

bool NoiseSuppressor::isEnabled() const {
    return enabled;
}

void NoiseSuppressor::setSampleRate(int sampleRate) {
    if (this->sampleRate != sampleRate) {
        this->sampleRate = sampleRate;
        reinitialize();
    }
}

void NoiseSuppressor::setBufferSize(int bufferSize) {
    if (this->bufferSize != bufferSize) {
        this->bufferSize = bufferSize;
        reinitialize();
    }
}

void NoiseSuppressor::reinitialize() {
    destroy();
    init();
}

void NoiseSuppressor::setSensitivity(int sensitivity) {
    this->sensitivity = sensitivity;
    speex_set_sensitivity(sensitivity);
}

extern "C" {
JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_noise_NoiseSuppressionHelper_init(JNIEnv *env,
                                                                          jobject type,
                                                                          jint bufferSize,
                                                                          jint sampleRate,
                                                                          jint sensitivity) {
    noiseSuppressor->bufferSize = bufferSize;
    noiseSuppressor->sampleRate = sampleRate;
    noiseSuppressor->sensitivity = sensitivity;
    noiseSuppressor->init();
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_noise_NoiseSuppressionHelper_setEnabled(JNIEnv *env,
                                                                                jobject type,
                                                                                jboolean enabled) {
    noiseSuppressor->enable(enabled ? 1 : 0);
}

JNIEXPORT jint JNICALL
Java_com_chkin_hearing_data_local_audio_noise_NoiseSuppressionHelper_process(JNIEnv *env,
                                                                             jobject type,
                                                                             jshortArray buffer) {
    short *data = env->GetShortArrayElements(buffer, nullptr);
    int vad = noiseSuppressor->process(data);
    env->ReleaseShortArrayElements(buffer, data, 0);
    return vad;
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_noise_NoiseSuppressionHelper_changeSensitivity(JNIEnv *env,
                                                                                       jobject type,
                                                                                       jint sensitivity) {
    noiseSuppressor->setSensitivity(sensitivity);
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_noise_NoiseSuppressionHelper_destroy(JNIEnv *env,
                                                                             jobject type) {
    noiseSuppressor->destroy();
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_noise_NoiseSuppressionHelper_reinitialize(JNIEnv *env,
                                                                                  jobject type,
                                                                                  jint bufferSize,
                                                                                  jint sampleRate,
                                                                                  jint sensitivity) {
    noiseSuppressor->bufferSize = bufferSize;
    noiseSuppressor->sampleRate = sampleRate;
    noiseSuppressor->sensitivity = sensitivity;
    noiseSuppressor->reinitialize();
}
}

