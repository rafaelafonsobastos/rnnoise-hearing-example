//
// Created by Roman Samoilenko on 2019-11-26.
//

#ifndef CHKIN_HEARING_NOISESUPPRESSOR_H
#define CHKIN_HEARING_NOISESUPPRESSOR_H

class NoiseSuppressor {
private:
    bool enabled;

public:

    int sampleRate;
    int bufferSize;
    int sensitivity;

    void init();

    void enable(int enabled);

    int process(short data[]);

    void destroy();

    bool isEnabled() const;

    void setSampleRate(int sampleRate);

    void setBufferSize(int bufferSize);

    void setSensitivity(int sensitivity);

    void reinitialize();
};

extern NoiseSuppressor *noiseSuppressor;

#endif //CHKIN_HEARING_NOISESUPPRESSOR_H
