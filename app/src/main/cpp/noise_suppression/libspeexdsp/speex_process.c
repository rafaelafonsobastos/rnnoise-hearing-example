#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdio.h>
#include <android/log.h>
#include "speex_process.h"
#include "speex/speex_preprocess.h"

SpeexPreprocessState *st;

void speex_init(int frame_size, int sample_rate, int sensitivity) {
    int i;
    float f;

    st = speex_preprocess_state_init(frame_size / 2, sample_rate);
    i = 1;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_ENABLED, &i);
    i = 1;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_DENOISE, &i);
    i = 0;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_AGC, &i);
    i = 8000;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_AGC_LEVEL, &i);
    i = 0;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_DEREVERB, &i);
    f = .0;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_DEREVERB_DECAY, &f);
    f = .0;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_DEREVERB_LEVEL, &f);
    i = sensitivity;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_NOISE_SUPPRESS, &i);
}

void speex_enable(int enabled) {
    int i = enabled;
    speex_preprocess_ctl(st, SPEEX_PREPROCESS_ENABLED, &i);
}

int speex_process(short data[]) {
    return speex_preprocess_run(st, data);
}

void speex_destroy() {
    if (st) {
        speex_preprocess_state_destroy(st);
        st = NULL;
    }
}

void speex_set_sensitivity(int sensitivity) {
    if (st) {
        int i = sensitivity;
        speex_preprocess_ctl(st, SPEEX_PREPROCESS_SET_NOISE_SUPPRESS, &i);
    }
}
