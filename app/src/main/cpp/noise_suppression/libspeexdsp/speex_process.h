//
// Created by Roman Samoilenko on 2019-11-26.
//

#ifndef CHKIN_HEARING_SPEEX_PROCESS_H
#define CHKIN_HEARING_SPEEX_PROCESS_H

void speex_init(int frame_size, int sample_rate, int sensitivity);

void speex_enable(int enabled);

int speex_process(short data[]);

void speex_set_sensitivity(int sensitivity);

void speex_destroy();

#endif //CHKIN_HEARING_SPEEX_PROCESS_H
