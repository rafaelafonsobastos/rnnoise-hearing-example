/**
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "LiveEffectEngine.h"
#include "../../../../oboe/src/common/OboeDebug.h"
#include <assert.h>

LiveEffectEngine::LiveEffectEngine() {
    assert(mOutputChannelCount == mInputChannelCount);
}

LiveEffectEngine::~LiveEffectEngine() {
    stop();
    closeStreams();
}

void LiveEffectEngine::setRecordingDeviceId(int32_t deviceId) {
    mRecordingDeviceId = deviceId;
}

void LiveEffectEngine::setPlaybackDeviceId(int32_t deviceId) {
    mPlaybackDeviceId = deviceId;
}

bool LiveEffectEngine::isAAudioSupported() {
    return oboe::AudioStreamBuilder::isAAudioRecommended();
}

bool LiveEffectEngine::setAudioApi(oboe::AudioApi api) {
    if (mIsEffectOn) {
        return false;
    }
    mAudioApi = api;
    return true;
}

void LiveEffectEngine::setEffectOn(bool isOn) {
    if (isOn != mIsEffectOn) {
        mIsEffectOn = isOn;
        if (isOn) {
            openStreams();
            start();
        } else {
            stop();
            closeStreams();
        }
    }
}

bool LiveEffectEngine::isEffectOn() {
    return mIsEffectOn;
}

void LiveEffectEngine::openStreams() {
    // Note: The order of stream creation is important. We create the playback
    // stream first, then use properties from the playback stream
    // (e.g. sample rate) to create the recording stream. By matching the
    // properties we should get the lowest latency path
    oboe::AudioStreamBuilder inBuilder;
    oboe::AudioStreamBuilder outBuilder;
    setupPlaybackStreamParameters(&outBuilder);
    outBuilder.openStream(&mPlayStream);
    warnIfNotLowLatency(mPlayStream);

    setupRecordingStreamParameters(&inBuilder);
    inBuilder.openStream(&mRecordingStream);
    warnIfNotLowLatency(mRecordingStream);

    adjustBufferSize(mRecordingStream, "RECORDING");
    adjustBufferSize(mPlayStream, "PLAYBACK");

    setInputStream(mRecordingStream);
    setOutputStream(mPlayStream);
}

void LiveEffectEngine::closeStreams() {
    /*
     * Note: The order of events is important here.
     * The playback stream must be closed before the recording stream. If the
     * recording stream were to be closed first the playback stream's
     * callback may attempt to read from the recording stream
     * which would cause the app to crash since the recording stream would be
     * null.
     */
    closeStream(mPlayStream);
    closeStream(mRecordingStream);
}

/**
 * Sets the stream parameters which are specific to recording,
 * including the sample rate which is determined from the
 * playback stream.
 *
 * @param builder The recording stream builder
 */
oboe::AudioStreamBuilder *LiveEffectEngine::setupRecordingStreamParameters(
        oboe::AudioStreamBuilder *builder) {
    // This sample uses blocking read() by setting callback to null
    builder->setCallback(nullptr)
            ->setDeviceId(mRecordingDeviceId)
            ->setInputPreset(mInputPreset)
            ->setDirection(oboe::Direction::Input)
            ->setSampleRate(mSampleRate)
            ->setChannelCount(mInputChannelCount);
    LOGE("RECORDING PARAMS %d, %d, %d, %d, %d",
         builder->getDeviceId(),
         builder->getInputPreset(),
         builder->getDirection(),
         builder->getSampleRate(),
         builder->getChannelCount());
    return setupCommonStreamParameters(builder);
}

/**
 * Sets the stream parameters which are specific to playback, including device
 * id and the dataCallback function, which must be set for low latency
 * playback.
 * @param builder The playback stream builder
 */
oboe::AudioStreamBuilder *LiveEffectEngine::setupPlaybackStreamParameters(
        oboe::AudioStreamBuilder *builder) {
    builder->setCallback(this)
            ->setDeviceId(mPlaybackDeviceId)
            ->setDirection(oboe::Direction::Output)
            ->setSampleRate(mSampleRate)
            ->setSessionId(oboe::SessionId::Allocate)
            ->setChannelCount(mOutputChannelCount);
    LOGE("PLAYBACK PARAMS %d, %d, %d, %d, %d",
         builder->getDeviceId(),
         builder->getDirection(),
         builder->getSampleRate(),
         builder->getSessionId(),
         builder->getChannelCount());
    return setupCommonStreamParameters(builder);
}

/**
 * Set the stream parameters which are common to both recording and playback
 * streams.
 * @param builder The playback or recording stream builder
 */
oboe::AudioStreamBuilder *LiveEffectEngine::setupCommonStreamParameters(
        oboe::AudioStreamBuilder *builder) {
    // We request EXCLUSIVE mode since this will give us the lowest possible
    // latency.
    // If EXCLUSIVE mode isn't available the builder will fall back to SHARED
    // mode.
    builder->setAudioApi(mAudioApi)
            ->setFormat(mFormat)
            ->setSharingMode(oboe::SharingMode::Exclusive)
            ->setPerformanceMode(oboe::PerformanceMode::LowLatency);
    return builder;
}

/**
 * Close the stream. AudioStream::close() is a blocking call so
 * the application does not need to add synchronization between
 * onAudioReady() function and the thread calling close().
 * [the closing thread is the UI thread in this sample].
 * @param stream the stream to close
 */
void LiveEffectEngine::closeStream(oboe::AudioStream *stream) {
    if (stream) {
        oboe::Result result = stream->close();
        if (result != oboe::Result::OK) {
            LOGE("Error closing stream. %s", oboe::convertToText(result));
        }
        LOGW("Successfully closed streams");
    }
}

float LiveEffectEngine::toAudioAmplificationMultiplier(float gain) {
    float minRatio = 1.0f;
    float maxRatio = 5.0f;

    float amplificationRange = maxRatio - minRatio;
    float amplification = minRatio + amplificationRange * gain;
    return amplification;
}

oboe::DataCallbackResult LiveEffectEngine::onBothStreamsReady(
        const void *inputData,
        int numInputFrames,
        void *outputData,
        int numOutputFrames) {
    size_t bytesPerFrame = this->getOutputStream()->getBytesPerFrame();
    size_t bytesToWrite = numInputFrames * bytesPerFrame;
    size_t byteDiff = (numOutputFrames - numInputFrames) * bytesPerFrame;
    size_t bytesToZero = (byteDiff > 0) ? byteDiff : 0;
    memcpy(outputData, inputData, bytesToWrite);
    memset((u_char *) outputData + bytesToWrite, 0, bytesToZero);


    auto *audio = static_cast<short *>(outputData);
    int size = numOutputFrames * mOutputChannelCount;
    analyzer->process(audio, size, false);
    if (equalizer->isEnabled() || bandpassFilter->isEnabled()/* || noiseSuppressor->isEnabled()*/) {
//        analyzer->process(audio, size, false);
        int bufferSize = mBufferSizeInBursts * mPlayStream->getFramesPerBurst();
//        LOGD("onBothStreamsReady, mSampleRate: %d, bufferSize: %d", mSampleRate, bufferSize);


        if (noiseSuppressor->isEnabled()) {
            noiseSuppressor->setSampleRate(mSampleRate);
            noiseSuppressor->setBufferSize(
                    mBufferSizeInBursts * mPlayStream->getFramesPerBurst());
//            noiseSuppressor->setSampleRate(48000);
//            noiseSuppressor->setBufferSize(384);
            noiseSuppressor->process(audio);
        }

        if (bandpassFilter->isEnabled()) {
            bandpassFilter->process(audio, size);
        }

        if (equalizer->isEnabled()) {
            equalizer->process(audio, size);
        }

//        analyzer->process(audio, size, true);
    }

    if (mAudioAmplification != 0.F || mLeftChannelGain != 0.F || mRightChannelGain != 0.F) {
        for (int i = 0; i < size; i++) {
            if (mInputChannelCount == 1) {
                audio[i] = limitToInt16(
                        audio[i] * toAudioAmplificationMultiplier(mAudioAmplification));
            } else if (mInputChannelCount == 2) {
                audio[i] = limitToInt16(audio[i]
                                        * (i % 2 == 0 ? toAudioAmplificationMultiplier(
                        mLeftChannelGain)
                                                      : toAudioAmplificationMultiplier(
                                mRightChannelGain)));
            }
        }
    }

    analyzer->process(audio, size, true);

    double power = calculatePowerDb(audio, 0, size);
    if (power > MIN_SIGNAL_POWER) {
        mLatestLoudnessInDb = power;
    }

    return oboe::DataCallbackResult::Continue;
}

void LiveEffectEngine::adjustBufferSize(oboe::AudioStream *stream, const char *name) {
    if (stream->getAudioApi() == oboe::AudioApi::AAudio) {
        int value = stream->getFramesPerBurst() * mBufferSizeInBursts;
        oboe::Result result = stream->setBufferSizeInFrames(value);

        LOGE("Adjust buffer size for %s, result = %s, requested = %d, actual = %d",
             name,
             oboe::convertToText(result),
             value,
             stream->getBufferSizeInFrames()
        );
    }
}

/**
 * Warn in logcat if non-low latency stream is created
 * @param stream: newly created stream
 *
 */
void LiveEffectEngine::warnIfNotLowLatency(oboe::AudioStream *stream) {
    if (stream->getPerformanceMode() != oboe::PerformanceMode::LowLatency) {
        LOGW(
                "Stream is NOT low latency."
                "Check your requested format, sample rate and channel count");
    }
}

/**
 * Oboe notifies the application for "about to close the stream".
 *
 * @param oboeStream: the stream to close
 * @param error: oboe's reason for closing the stream
 */
void LiveEffectEngine::onErrorBeforeClose(oboe::AudioStream *oboeStream,
                                          oboe::Result error) {
    LOGE("%s stream Error before close: %s",
         oboe::convertToText(oboeStream->getDirection()),
         oboe::convertToText(error));
}

/**
 * Oboe notifies application that "the stream is closed"
 *
 * @param oboeStream
 * @param error
 */
void LiveEffectEngine::onErrorAfterClose(oboe::AudioStream *oboeStream,
                                         oboe::Result error) {
    LOGE("%s stream Error after close: %s",
         oboe::convertToText(oboeStream->getDirection()),
         oboe::convertToText(error));
    // FIXME this is error-prone. ErrorDisconnected happens when the last headset is disconnected
    // thus the app may continue to play audio without any headset
//    if (oboeStream->getAudioApi() == oboe::AudioApi::AAudio &&
//        error == oboe::Result::ErrorDisconnected) {
//        setEffectOn(false);
//        setEffectOn(true);
//    }
}

void LiveEffectEngine::setSampleRate(int32_t sampleRate) {
    mSampleRate = sampleRate;
}

void LiveEffectEngine::setInputPreset(int32_t inputPreset) {
    mInputPreset = static_cast<oboe::InputPreset>(inputPreset);
}

void LiveEffectEngine::setChannelCount(int32_t channelCount) {
    mInputChannelCount = channelCount;
    mOutputChannelCount = channelCount;
}

void LiveEffectEngine::setBufferSizeInBursts(int32_t bufferSizeInBursts) {
    mBufferSizeInBursts = bufferSizeInBursts;
}

void LiveEffectEngine::setAudioAmplification(float value) {
    mAudioAmplification = value;
}

void LiveEffectEngine::setLeftChannelGain(float value) {
    mLeftChannelGain = value;
}

void LiveEffectEngine::setRightChannelGain(float value) {
    mRightChannelGain = value;
}

int LiveEffectEngine::getUnderrunCount() {
    return mPlayStream ? mPlayStream->getXRunCount().value() : -1;
}

int LiveEffectEngine::getOverrunCount() {
    return mRecordingStream ? mRecordingStream->getXRunCount().value() : -1;
}

int LiveEffectEngine::getAudioSessionId() {
    return mPlayStream ? mPlayStream->getSessionId() : -1;
}

double LiveEffectEngine::getAudioLoudnessInDb() {
    if (mPlayStream) {
        return mLatestLoudnessInDb;
    } else {
        return std::numeric_limits<double>::lowest();
    }
}

LiveEffectEngine::CalibrationConclusionData LiveEffectEngine::calibrateStreamsConfiguration() {
    std::vector<oboe::AudioApi> apiTypes = {oboe::AudioApi::AAudio, oboe::AudioApi::OpenSLES};
    if (!isAAudioSupported()) {
        apiTypes.erase(apiTypes.begin());
    }
    oboe::ChannelCount channels[] = {oboe::ChannelCount::Stereo, oboe::ChannelCount::Mono};
    oboe::InputPreset inputPresets[] = {
            oboe::InputPreset::Generic,
            oboe::InputPreset::VoiceCommunication,
            oboe::InputPreset::VoicePerformance,
            oboe::InputPreset::VoiceRecognition
    };
    int sampleRates[] = {48000, 44100};

    CalibrationResult result = CalibrationResult::NO_RESULT;
    oboe::AudioApi bestApiType = oboe::AudioApi::Unspecified;
    oboe::ChannelCount bestChannel = oboe::ChannelCount::Unspecified;
    oboe::InputPreset bestInputPreset = oboe::InputPreset::Generic;
    int bestSampleRate = -1;

    for (auto &apiType: apiTypes) {
        for (auto &inputPreset: inputPresets) {
            for (auto &channel: channels) {
                for (int sampleRate: sampleRates) {
                    mAudioApi = apiType;
                    mInputPreset = inputPreset;
                    mInputChannelCount = mOutputChannelCount = channel;
                    mSampleRate = sampleRate;

                    openStreams();

                    if (mPlayStream && mRecordingStream) {
                        oboe::PerformanceMode playStreamPerformance =
                                mPlayStream->getPerformanceMode();
                        oboe::PerformanceMode recordStreamPerformance =
                                mRecordingStream->getPerformanceMode();

                        if (playStreamPerformance == oboe::PerformanceMode::LowLatency
                            && recordStreamPerformance == oboe::PerformanceMode::LowLatency
                            && result < CalibrationResult::RESULT_BOTH) {
                            bestApiType = apiType;
                            bestInputPreset = inputPreset;
                            bestChannel = channel;
                            bestSampleRate = sampleRate;
                            result = CalibrationResult::RESULT_BOTH;
                        } else if (playStreamPerformance == oboe::PerformanceMode::LowLatency
                                   && result < CalibrationResult::RESULT_ONLY_PLAYBACK) {
                            bestApiType = apiType;
                            bestInputPreset = inputPreset;
                            bestChannel = channel;
                            bestSampleRate = sampleRate;
                            result = CalibrationResult::RESULT_ONLY_PLAYBACK;
                        } else if (recordStreamPerformance == oboe::PerformanceMode::LowLatency
                                   && result < CalibrationResult::RESULT_ONLY_RECORDING) {
                            bestApiType = apiType;
                            bestInputPreset = inputPreset;
                            bestChannel = channel;
                            bestSampleRate = sampleRate;
                            result = CalibrationResult::RESULT_ONLY_RECORDING;
                        } else if (result < CalibrationResult::RESULT_NEITHER) {
                            bestApiType = apiType;
                            bestInputPreset = inputPreset;
                            bestChannel = channel;
                            bestSampleRate = sampleRate;
                            result = CalibrationResult::RESULT_NEITHER;
                        }
                    }
                    closeStreams();

                    if (result == CalibrationResult::RESULT_BOTH) {
                        mAudioApi = bestApiType;
                        mInputPreset = bestInputPreset;
                        mInputChannelCount = mOutputChannelCount = bestChannel;
                        mSampleRate = bestSampleRate;
                        return {result, bestApiType, bestInputPreset, bestChannel, bestSampleRate};
                    }
                }
            }
        }
    }

    mAudioApi = bestApiType;
    mInputPreset = bestInputPreset;
    mInputChannelCount = mOutputChannelCount = bestChannel;
    mSampleRate = bestSampleRate;
    return {result, bestApiType, bestInputPreset, bestChannel, bestSampleRate};
}

int16_t LiveEffectEngine::limitToInt16(float value) {
    if (value > SHRT_MAX) {
        return SHRT_MAX;
    } else if (value < SHRT_MIN) {
        return SHRT_MIN;
    } else {
        return static_cast<int16_t>(value);
    }
}

double LiveEffectEngine::calculatePowerDb(short *data, int offset, int samples) {
    // Calculate the sum of the values, and the sum of the squared values.
    // We need longs to avoid running out of bits.
    double sum = 0.0;
    double sqsum = 0.0;
    for (int i = 0; i < samples; i++) {
        long entry = *(data + offset + i);
        sum += entry;
        sqsum += (entry * entry);
    }
    // sqsum is the sum of all (signal+bias)¬≤, so
    //     sqsum = sum(signal¬≤) + samples * bias¬≤
    // hence
    //     sum(signal¬≤) = sqsum - samples * bias¬≤
    // Bias is simply the average value, i.e.
    //     bias = sum / samples
    // Since power = sum(signal¬≤) / samples, we have
    //     power = (sqsum - samples * sum¬≤ / samples¬≤) / samples
    // so
    //     power = (sqsum - sum¬≤ / samples) / samples
    double power = (sqsum - sum * sum / samples) / samples;

    // Scale to the range 0 - 1.
    power /= (MAX_16BIT * MAX_16BIT);

    // Convert to dB, with 0 being max power.  Add a fudge factor to make
    // a "real" fully saturated input come to 0 dB.
    return log10(power) * 10.0 + 0.6;
}