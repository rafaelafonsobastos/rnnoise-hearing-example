/*
 * Copyright 2018 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OBOE_LIVEEFFECTENGINE_H
#define OBOE_LIVEEFFECTENGINE_H

#include <jni.h>
#include <oboe/Oboe.h>
#include <string>
#include <thread>
#include <vector>
#include <cmath>
#include "FullDuplexStream.h"
#include "equalizer/Equalizer.h"
#include "analyzer/Analyzer.h"
#include "bandpass/BandpassFilter.h"
#include "noise_suppression/NoiseSuppressor.h"

#define MAX_16BIT 32768
#define MIN_SIGNAL_POWER (-120)

class LiveEffectEngine : public FullDuplexStream {
public:
    enum class CalibrationResult : int32_t {
        NO_RESULT = -1,
        RESULT_NEITHER = 0,
        RESULT_ONLY_RECORDING = 1,
        RESULT_ONLY_PLAYBACK = 2,
        RESULT_BOTH = 3
    };

    class CalibrationConclusionData {
    public:
        CalibrationResult result;
        oboe::AudioApi apiType;
        oboe::InputPreset inputPreset;
        oboe::ChannelCount channelCount;
        int sampleRate;

        CalibrationConclusionData(
                CalibrationResult result,
                oboe::AudioApi api,
                oboe::InputPreset inputPreset,
                oboe::ChannelCount channels,
                int rate
        ) {
            this->result = result;
            this->apiType = api;
            this->inputPreset = inputPreset;
            this->channelCount = channels;
            this->sampleRate = rate;
        }
    };

    LiveEffectEngine();

    ~LiveEffectEngine();

    void setRecordingDeviceId(int32_t deviceId);

    void setPlaybackDeviceId(int32_t deviceId);

    void setEffectOn(bool isOn);

    virtual oboe::DataCallbackResult onBothStreamsReady(
            const void *inputData,
            int numInputFrames,
            void *outputData,
            int numOutputFrames);

    /*
     * oboe::AudioStreamCallback interface implementation
     */
    void onErrorBeforeClose(oboe::AudioStream *oboeStream, oboe::Result error);

    void onErrorAfterClose(oboe::AudioStream *oboeStream, oboe::Result error);

    bool setAudioApi(oboe::AudioApi);

    void setSampleRate(int32_t sampleRate);

    void setInputPreset(int32_t inputPreset);

    void setChannelCount(int32_t channelCount);

    void setBufferSizeInBursts(int32_t bufferSizeInBursts);

    void setAudioAmplification(float value);

    void setLeftChannelGain(float value);

    void setRightChannelGain(float value);

    bool isAAudioSupported(void);

    bool isEffectOn();

    int getUnderrunCount();

    int getOverrunCount();

    CalibrationConclusionData calibrateStreamsConfiguration();

    int getAudioSessionId();

    double getAudioLoudnessInDb();

private:
    bool mIsEffectOn = false;
    int32_t mRecordingDeviceId = oboe::kUnspecified;
    int32_t mPlaybackDeviceId = oboe::kUnspecified;
    oboe::AudioFormat mFormat = oboe::AudioFormat::I16;
    int32_t mSampleRate = oboe::kUnspecified;
    oboe::InputPreset mInputPreset = oboe::InputPreset::VoiceRecognition;
    int32_t mInputChannelCount = oboe::ChannelCount::Stereo;
    int32_t mOutputChannelCount = oboe::ChannelCount::Stereo;
    int32_t mBufferSizeInBursts = 2;
    float mAudioAmplification = 0.f;
    float mLeftChannelGain = 0.f;
    float mRightChannelGain = 0.f;
    double mLatestLoudnessInDb = std::numeric_limits<double>::lowest();

    oboe::AudioStream *mRecordingStream = nullptr;
    oboe::AudioStream *mPlayStream = nullptr;
    oboe::AudioApi mAudioApi = oboe::AudioApi::AAudio;

    void openStreams();

    void closeStreams();

    void closeStream(oboe::AudioStream *stream);

    float toAudioAmplificationMultiplier(float gain);

    oboe::AudioStreamBuilder *setupCommonStreamParameters(
            oboe::AudioStreamBuilder *builder);

    oboe::AudioStreamBuilder *setupRecordingStreamParameters(
            oboe::AudioStreamBuilder *builder);

    oboe::AudioStreamBuilder *setupPlaybackStreamParameters(
            oboe::AudioStreamBuilder *builder);

    void warnIfNotLowLatency(oboe::AudioStream *stream);

    void adjustBufferSize(oboe::AudioStream *stream, const char *name);

    double calculatePowerDb(short *data, int offset, int samples);

    int16_t limitToInt16(float value);
};

#endif  // OBOE_LIVEEFFECTENGINE_H
