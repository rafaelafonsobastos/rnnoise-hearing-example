

#ifndef CHKIN_HEARING_ANALYZER_H
#define CHKIN_HEARING_ANALYZER_H

#include "data/ObservableShortArray.h"
#include "fft/FFT.h"
#include "filters/LowPassFilter.h"
#include "filters/HammingWindow.h"
#include "jni.h"

class Analyzer {
public:
    static const int FFT_SIZE = 1024;

    void setEnabled(bool enabled);

    void process(short *data, int size, bool postEqualization);

    double *getLatestMagnitudes();

private:
    FFT *fft = new FFT(FFT_SIZE);

    bool isEnabled = false;

    // BEFORE = 0..<512, AFTER 512..<1024
    double latestMagnitudes[FFT_SIZE] = {0.0};

    double latestMagnitudesBeforeInternal[FFT_SIZE / 2] = {0.0};

    LowPassFilter *lpfBefore = new LowPassFilter();

    LowPassFilter *lpfAfter = new LowPassFilter();

    ObservableShortArray *arrayBefore =
            new ObservableShortArray(FFT_SIZE, [this](short *values, int size) {
                this->callbackBefore(values, size);
            });

    ObservableShortArray *arrayAfter =
            new ObservableShortArray(FFT_SIZE, [this](short *values, int size) {
                this->callbackAfter(values, size);
            });

    void callbackBefore(short *values, int size);

    void callbackAfter(short *values, int size);

};

extern Analyzer *analyzer;

#endif //CHKIN_HEARING_ANALYZER_H
