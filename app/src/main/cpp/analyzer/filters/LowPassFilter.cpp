
#include "LowPassFilter.h"

double LowPassFilter::process(double value) {
    double output = (value + lastValue) / 2.0;
    lastValue = value;
    return output;
}

void LowPassFilter::process(double *array, int size) {
    for (int i = 0; i < size; i++) {
        array[i] = process(array[i]);
    }
}

void LowPassFilter::resetLastValue() {
    lastValue = 0.0;
}
