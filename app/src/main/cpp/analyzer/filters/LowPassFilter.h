

#ifndef CHKIN_HEARING_LOWPASSFILTER_H
#define CHKIN_HEARING_LOWPASSFILTER_H

class LowPassFilter {
private:
    double lastValue = 0.0;

public:
    double process(double value);

    void process(double *array, int size);

    void resetLastValue();
};


#endif //CHKIN_HEARING_LOWPASSFILTER_H
