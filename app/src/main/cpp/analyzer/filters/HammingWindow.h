
#ifndef CHKIN_HEARING_HAMMINGWINDOW_H
#define CHKIN_HEARING_HAMMINGWINDOW_H

#include <cmath>

const double TWO_PI = 2.0 * M_PI;

double coefficient(int length, int index);

void process(double *array, int size);

#endif //CHKIN_HEARING_HAMMINGWINDOW_H
