
#include "HammingWindow.h"

double coefficient(int length, int index) {
    return 0.54 - 0.46 * cos(TWO_PI * index / (length - 1));
}

void process(double *array, int size) {
    for (int i = 0; i < size; i++) {
        array[i] *= coefficient(size, i);
    }
}