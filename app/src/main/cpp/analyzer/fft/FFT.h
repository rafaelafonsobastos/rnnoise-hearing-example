

#ifndef CHKIN_HEARING_FFT_H
#define CHKIN_HEARING_FFT_H

#include <cmath>
#include <stdexcept>

class FFT {
private:
    int n;
    int m;

    // Lookup tables. Only need to recompute when size of FFT changes.
    double *cosine;
    double *sine;

public:
    explicit FFT(int n);

    ~FFT();

    void process(double *x, double *y);
};


#endif //CHKIN_HEARING_FFT_H
