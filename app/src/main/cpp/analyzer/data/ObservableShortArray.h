

#ifndef CHKIN_HEARING_OBSERVABLESHORTARRAY_H
#define CHKIN_HEARING_OBSERVABLESHORTARRAY_H

#include <functional>
#include <utility>

class ObservableShortArray {
private:
    std::function<void (short*, int)> callback;

    int capacity, index = 0;

    short *array;

    void checkIndex();

public:
    explicit ObservableShortArray(int capacity, std::function<void (short*, int)> callback);

    ~ObservableShortArray();

    void add(short *values, int size);

    void clear();
};


#endif //CHKIN_HEARING_OBSERVABLESHORTARRAY_H
