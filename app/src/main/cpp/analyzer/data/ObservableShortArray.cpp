
#include "ObservableShortArray.h"

ObservableShortArray::ObservableShortArray(
        int capacity,
        std::function<void(short *, int)> callback
) : capacity(capacity), callback(std::move(callback)) {
    array = new short[capacity * 16];
}

ObservableShortArray::~ObservableShortArray() {
    delete[](array);
}

void ObservableShortArray::checkIndex() {
    while (index >= capacity) {
        auto *out = new short[capacity];
        memcpy(out, array + 0, sizeof(short) * capacity);
        callback(out, capacity);
        index -= capacity;
        delete[](out);
    }
}

void ObservableShortArray::add(short *values, int size) {
    for (int i = index; i < (index + size); i++) {
        array[i] = values[i - index];
    }
    index += size;
    checkIndex();
}

void ObservableShortArray::clear() {
    if (index > 0) {
        for (int i = 0; i < index; i++) {
            array[i] = 0;
        }
        index = 0;
    }
}
