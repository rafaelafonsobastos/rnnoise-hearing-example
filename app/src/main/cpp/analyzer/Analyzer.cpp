
#include "Analyzer.h"

Analyzer *analyzer = new Analyzer();

void Analyzer::callbackBefore(short *values, int size) {
    double filteredRealValues[FFT_SIZE];
    for (int i = 0; i < FFT_SIZE; i++) {
        filteredRealValues[i] = lpfBefore->process((double) values[i]) * coefficient(FFT_SIZE, i);
    }
    double imaginaryValues[FFT_SIZE] = {0.0};
    fft->process(filteredRealValues, imaginaryValues);
    for (int i = 0; i < FFT_SIZE / 2; i++) {
//        latestMagnitudesBeforeInternal[i] = sqrt(
//                filteredRealValues[i] * filteredRealValues[i] + imaginaryValues[i] * imaginaryValues[i]
//        );
        latestMagnitudesBeforeInternal[i] = 10 * log10(
                filteredRealValues[i] * filteredRealValues[i] + imaginaryValues[i] * imaginaryValues[i]
        );
    }
}

void Analyzer::callbackAfter(short *values, int size) {
    double filteredRealValues[FFT_SIZE];
    for (int i = 0; i < FFT_SIZE; i++) {
        filteredRealValues[i] = lpfAfter->process((double) values[i]) * coefficient(FFT_SIZE, i);
    }
    double imaginaryValues[FFT_SIZE] = {0.0};
    fft->process(filteredRealValues, imaginaryValues);
    for (int i = 0; i < FFT_SIZE / 2; i++) {
        latestMagnitudes[i] = latestMagnitudesBeforeInternal[i];

//        magnitude = sqrt(pow(cx_out[position].r, 2) + pow(cx_out[position].i, 2));
//        double dB = 10 * log10(pow(cx_out[position].r, 2) + pow(cx_out[position].i, 2));

//        latestMagnitudes[i + (FFT_SIZE / 2)] = sqrt(
//                filteredRealValues[i] * filteredRealValues[i] + imaginaryValues[i] * imaginaryValues[i]
//        );
        latestMagnitudes[i + (FFT_SIZE / 2)] = 10 * log10(
                filteredRealValues[i] * filteredRealValues[i] + imaginaryValues[i] * imaginaryValues[i]
        );
    }
}

void Analyzer::setEnabled(bool enabled) {
    isEnabled = enabled;
    if (!enabled) {
        arrayBefore->clear();
        arrayAfter->clear();
    }
}

void Analyzer::process(short *data, int size, bool postEqualization) {
    if (isEnabled) {
        if (postEqualization) {
            arrayAfter->add(data, size);
        } else {
            arrayBefore->add(data, size);
        }
    }
}

double *Analyzer::getLatestMagnitudes() {
    return latestMagnitudes;
}

extern "C" {
JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_analyzer_AnalyzerHelper_setEnabled(JNIEnv *env,
                                                                           jobject,
                                                                           jboolean enabled) {
    analyzer->setEnabled(enabled);
}

JNIEXPORT void JNICALL
Java_com_chkin_hearing_data_local_audio_analyzer_AnalyzerHelper_process(JNIEnv *env,
                                                                        jobject,
                                                                        jshortArray data,
                                                                        jint size,
                                                                        jboolean postEqualization) {
    short *array = env->GetShortArrayElements(data, nullptr);
    analyzer->process(array, size, postEqualization);
    env->ReleaseShortArrayElements(data, array, 0);
}

JNIEXPORT jdoubleArray JNICALL
Java_com_chkin_hearing_data_local_audio_analyzer_AnalyzerHelper_getAudioMagnitudes(JNIEnv *env,
                                                                                   jobject) {
    jdoubleArray array = env->NewDoubleArray(Analyzer::FFT_SIZE);
    env->SetDoubleArrayRegion(array, 0, Analyzer::FFT_SIZE, analyzer->getLatestMagnitudes());
    return array;
}
}