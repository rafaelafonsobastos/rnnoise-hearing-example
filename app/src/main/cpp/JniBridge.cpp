#include "../../../../oboe/src/common/OboeDebug.h"
#include "LiveEffectEngine.h"

static const int kApiAAudio = 0;
static const int kApiOpenSLES = 1;

static LiveEffectEngine *engine = nullptr;

extern "C" {

    JNIEXPORT jboolean JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_create(JNIEnv *env,
                                                                         jobject) {
        if (engine == nullptr) {
            engine = new LiveEffectEngine();
        }

        return (engine != nullptr);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_delete(JNIEnv *env,
                                                                         jobject) {
        if (engine == nullptr) {
            return;
        }
        engine->setEffectOn(false);
        delete engine;
        engine = nullptr;
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setActive(JNIEnv *env,
                                                                            jobject,
                                                                            jboolean active) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setActiveNative method");
            return;
        }

        engine->setEffectOn(active);
    }

    JNIEXPORT jobject JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_calibrate(JNIEnv *env,
                                                                            jobject) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling calibrate method");
            return nullptr;
        }

        LiveEffectEngine::CalibrationConclusionData result = engine->calibrateStreamsConfiguration();

        jclass javaLocalClass = env->FindClass("com/chkin/hearing/data/model/local/audio/AudioCalibrationModel");
        auto javaGlobalClass = reinterpret_cast<jclass>(env->NewGlobalRef(javaLocalClass));
        jmethodID constructor = env->GetMethodID(javaGlobalClass, "<init>", "(IIIII)V");

        return env->NewObject(
                javaGlobalClass, constructor,
                (int32_t) result.result,
                (int32_t) result.apiType,
                (int32_t) result.inputPreset,
                (int32_t) result.channelCount,
                result.sampleRate
        );
    }

    JNIEXPORT jboolean JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_isActive(JNIEnv *env,
                                                                           jobject) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling isActive method");
            return JNI_FALSE;
        }

        return static_cast<jboolean>(engine->isEffectOn() ? JNI_TRUE
                                                          : JNI_FALSE);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setRecordingDeviceId(JNIEnv *env,
                                                                                       jobject,
                                                                                       jint deviceId) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setRecordingDeviceId method");
            return;
        }

        engine->setRecordingDeviceId(deviceId);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setPlaybackDeviceId(JNIEnv *env,
                                                                                      jobject,
                                                                                      jint deviceId) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setPlaybackDeviceId method");
            return;
        }

        engine->setPlaybackDeviceId(deviceId);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setApiType(JNIEnv *env,
                                                                             jobject type,
                                                                             jint apiType) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setApiType method");
            return;
        }

        oboe::AudioApi audioApi;
        switch (apiType) {
            case kApiAAudio:
                audioApi = oboe::AudioApi::AAudio;
                break;
            case kApiOpenSLES:
                audioApi = oboe::AudioApi::OpenSLES;
                break;
            default:
                LOGE("Unknown API selection to setApiType() %d", apiType);
                return;
        }

        engine->setAudioApi(audioApi);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setSampleRate(JNIEnv *env,
                                                                                jobject type,
                                                                                jint sampleRate) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setSampleRate method");
            return;
        }

        engine->setSampleRate(sampleRate);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setInputPreset(JNIEnv *env,
                                                                                 jobject type,
                                                                                 jint inputPreset) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setInputPreset method");
            return;
        }

        engine->setInputPreset(inputPreset);
    }

    JNIEXPORT jboolean JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_isAAudioSupported(JNIEnv *env,
                                                                                    jobject type) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling isAAudioSupported method");
            return JNI_FALSE;
        }
        return static_cast<jboolean>(engine->isAAudioSupported() ? JNI_TRUE
                                                                 : JNI_FALSE);
    }

    JNIEXPORT jint JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_getUnderrunCount(JNIEnv *env,
                                                                                   jobject type) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling getUnderrunCount method");
            return -1;
        }
        return static_cast<jint>(engine->getUnderrunCount());
    }

    JNIEXPORT jint JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_getOverrunCount(JNIEnv *env,
                                                                                  jobject type) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling getOverrunCount method");
            return -1;
        }
        return static_cast<jint>(engine->getOverrunCount());
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setChannelCount(JNIEnv *env,
                                                                                  jobject type,
                                                                                  jint channelCount) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setChannelCount method");
            return;
        }

        engine->setChannelCount(channelCount);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setBufferSizeInBursts(JNIEnv *env,
                                                                                        jobject type,
                                                                                        jint bufferSizeInBursts) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setBufferSizeInBursts method");
            return;
        }

        engine->setBufferSizeInBursts(bufferSizeInBursts);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setAudioAmplification(JNIEnv *env,
                                                                                        jobject type,
                                                                                        jfloat value) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setAudioAmplification method");
            return;
        }

        engine->setAudioAmplification(value);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setLeftChannelGain(JNIEnv *env,
                                                                                     jobject type,
                                                                                     jfloat value) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setLeftChannelGain method");
            return;
        }

        engine->setLeftChannelGain(value);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_setRightChannelGain(JNIEnv *env,
                                                                                      jobject type,
                                                                                      jfloat value) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling setRightChannelGain method");
            return;
        }

        engine->setRightChannelGain(value);
    }

    JNIEXPORT jint JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_getAudioSessionId(JNIEnv *env,
                                                                                    jobject type) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling getAudioSessionId method");
            return -1;
        }
        return static_cast<jint>(engine->getAudioSessionId());
    }

    JNIEXPORT jdouble JNICALL
    Java_com_chkin_hearing_data_local_audio_AudioEngineNativeImpl_getAudioLoudnessInDb(JNIEnv *env,
                                                                                       jobject type) {
        if (engine == nullptr) {
            LOGE("Engine is null, you must call createEngine() before calling getAudioLoudnessInDb method");
            return -1;
        }
        return static_cast<jdouble>(engine->getAudioLoudnessInDb());
    }
}