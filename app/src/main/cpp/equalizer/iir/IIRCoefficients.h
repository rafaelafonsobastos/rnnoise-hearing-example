//
// Created by Roman Samoilenko on 3/4/19.
//

#ifndef CHKIN_HEARING_IIRCOEFFICIENTS_H
#define CHKIN_HEARING_IIRCOEFFICIENTS_H

struct IIRCoefficients {
    double beta;
    double alpha;
    double gamma;
};

#endif //CHKIN_HEARING_IIRCOEFFICIENTS_H
