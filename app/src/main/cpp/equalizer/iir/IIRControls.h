//
// Created by Roman Samoilenko on 3/4/19.
//
#ifndef CHKIN_HEARING_IIRCONTROLS_H
#define CHKIN_HEARING_IIRCONTROLS_H

#include <cmath>

class IIRControls {
public:
    static const int FREQUENCY_31 = 31;
    static const int FREQUENCY_62 = 62;
    static const int FREQUENCY_125 = 125;
    static const int FREQUENCY_250 = 250;
    static const int FREQUENCY_500 = 500;
    static const int FREQUENCY_1000 = 1000;
    static const int FREQUENCY_2000 = 2000;
    static const int FREQUENCY_4000 = 4000;
    static const int FREQUENCY_8000 = 8000;
    static const int FREQUENCY_16000 = 16000;

    static int get10BandIndex(int frequency) {
        switch (frequency) {
            case FREQUENCY_31:
                return 0;
            case FREQUENCY_62:
                return 1;
            case FREQUENCY_125:
                return 2;
            case FREQUENCY_250:
                return 3;
            case FREQUENCY_500:
                return 4;
            case FREQUENCY_1000:
                return 5;
            case FREQUENCY_2000:
                return 6;
            case FREQUENCY_4000:
                return 7;
            case FREQUENCY_8000:
                return 8;
            case FREQUENCY_16000:
                return 9;
            default:
                return -1;
        }
    }

    explicit IIRControls(int size);

    ~IIRControls();

    void setBandDbValue(int band, double value);

    int getSize() const;

    double *getBands() const;

private:
    int size;
    double *bands;
};

#endif //CHKIN_HEARING_IIRCONTROLS_H
