//
// Created by Roman Samoilenko on 4/4/19.
//

#include "IIREqualizer.h"

static const IIRCoefficients iir_cf10_44100[] = {
        /* 31 Hz*/
        {9.9688176273e-01, 1.5591186337e-03, 1.9968622855e+00},
        /* 62 Hz*/
        {9.9377323686e-01, 3.1133815717e-03, 1.9936954495e+00},
        /* 125 Hz*/
        {9.8748575691e-01, 6.2571215431e-03, 1.9871705722e+00},
        /* 250 Hz*/
        {9.7512812040e-01, 1.2435939802e-02, 1.9738753198e+00},
        /* 500 Hz*/
        {9.5087485437e-01, 2.4562572817e-02, 1.9459267562e+00},
        /* 1k Hz*/
        {9.0416308662e-01, 4.7918456688e-02, 1.8848691023e+00},
        /* 2k Hz*/
        {8.1751373987e-01, 9.1243130064e-02, 1.7442229115e+00},
        /* 4k Hz*/
        {6.6840529852e-01, 1.6579735074e-01, 1.4047189863e+00},
        /* 8k Hz*/
        {4.4858358977e-01, 2.7570820511e-01, 6.0517475334e-01},
        /* 16k Hz*/
        {2.4198119087e-01, 3.7900940457e-01, -8.0845085113e-01}
};
static const IIRCoefficients iir_cf10_48000[] = {
        /* 31 Hz*/
        {9.9713475915e-01, 1.4326204244e-03, 1.9971183163e+00},
        /* 62 Hz*/
        {9.9427771143e-01, 2.8611442874e-03, 1.9942120343e+00},
        /* 125 Hz*/
        {9.8849666727e-01, 5.7516663664e-03, 1.9882304829e+00},
        /* 250 Hz*/
        {9.7712566171e-01, 1.1437169144e-02, 1.9760670839e+00},
        /* 500 Hz*/
        {9.5477456091e-01, 2.2612719547e-02, 1.9505892385e+00},
        /* 1k Hz*/
        {9.1159452679e-01, 4.4202736607e-02, 1.8952405706e+00},
        /* 2k Hz*/
        {8.3100647694e-01, 8.4496761532e-02, 1.7686164442e+00},
        /* 4k Hz*/
        {6.9062328809e-01, 1.5468835596e-01, 1.4641227157e+00},
        /* 8k Hz*/
        {4.7820368352e-01, 2.6089815824e-01, 7.3910184176e-01},
        /* 16k Hz*/
        {2.5620076154e-01, 3.7189961923e-01, -6.2810038077e-01}
};

IIREqualizer::IIREqualizer(
        int bands,
        int sampleRate,
        int channelCount
) : bands(bands), sampleRate(sampleRate), channelCount(channelCount) {
    controlsLeft = new IIRControls(bands);
    controlsRight = new IIRControls(bands);
    init();
}

void IIREqualizer::init() {
    for (auto &i : dataHistoryLeft) {
        i = new XYData();
    }
    iLeft = 0;
    jLeft = 2;
    kLeft = 1;

    for (auto &i : dataHistoryRight) {
        i = new XYData();
    }
    iRight = 0;
    jRight = 2;
    kRight = 1;
}

void IIREqualizer::processMono(int16_t *data, int length){
    int band;
    double *eqbands = controlsLeft->getBands();
    double pcm;
    double out;

    XYData *tempd;
    for (int index = 0; index < length; index++) {
        pcm = data[index];

        out = 0.f;
        for (band = 0; band < bands; band++) {
            tempd = dataHistoryLeft[band];
            tempd->x[iLeft] = pcm;
            IIRCoefficients tempcf =
                    sampleRate == EQ_48000_RATE ? iir_cf10_48000[band]
                                                : iir_cf10_44100[band];
            tempd->y[iLeft] =
                    (
                            tempcf.alpha * (pcm - tempd->x[kLeft]) +
                            tempcf.gamma * tempd->y[jLeft] -
                            tempcf.beta * tempd->y[kLeft]
                    );

            out += (tempd->y[iLeft] * eqbands[band]);
        }
        out += (pcm * 0.25);

        out *= 4;

        data[index] = limitToShort(out);

        iLeft++;
        jLeft++;
        kLeft++;

        if (iLeft == 3)
            iLeft = 0;
        else if (jLeft == 3)
            jLeft = 0;
        else
            kLeft = 0;
    }
}

void IIREqualizer::processLeftSide(int16_t *data, int length) {
    int band;
    double *eqbands = controlsLeft->getBands();
    double pcm;
    double out;

    XYData *tempd;
    for (int index = 0; index < length; index += 2) {
        pcm = data[index];

        out = 0.f;
        for (band = 0; band < bands; band++) {
            tempd = dataHistoryLeft[band];
            tempd->x[iLeft] = pcm;
            IIRCoefficients tempcf =
                    sampleRate == EQ_48000_RATE ? iir_cf10_48000[band]
                                                : iir_cf10_44100[band];
            tempd->y[iLeft] =
                    (
                            tempcf.alpha * (pcm - tempd->x[kLeft]) +
                            tempcf.gamma * tempd->y[jLeft] -
                            tempcf.beta * tempd->y[kLeft]
                    );

            out += (tempd->y[iLeft] * eqbands[band]);
        }
        out += (pcm * 0.25);

        out *= 4;

        data[index] = limitToShort(out);

        iLeft++;
        jLeft++;
        kLeft++;

        if (iLeft == 3)
            iLeft = 0;
        else if (jLeft == 3)
            jLeft = 0;
        else
            kLeft = 0;
    }
}

void IIREqualizer::processRightSide(int16_t *data, int length) {
    int band;
    double *eqbands = controlsRight->getBands();
    double pcm;
    double out;

    XYData *tempd;
    for (int index = 1; index < length; index += 2) {
        pcm = data[index];

        out = 0.f;
        for (band = 0; band < bands; band++) {
            tempd = dataHistoryRight[band];
            tempd->x[iRight] = pcm;
            IIRCoefficients tempcf =
                    sampleRate == EQ_48000_RATE ? iir_cf10_48000[band]
                                                : iir_cf10_44100[band];
            tempd->y[iRight] =
                    (
                            tempcf.alpha * (pcm - tempd->x[kRight]) +
                            tempcf.gamma * tempd->y[jRight] -
                            tempcf.beta * tempd->y[kRight]
                    );

            out += (tempd->y[iRight] * eqbands[band]);
        }
        out += (pcm * 0.25);

        out *= 4;

        data[index] = limitToShort(out);

        iRight++;
        jRight++;
        kRight++;

        if (iRight == 3)
            iRight = 0;
        else if (jRight == 3)
            jRight = 0;
        else
            kRight = 0;
    }
}

void IIREqualizer::process(int16_t data[], int length) {
    if (channelCount == EQ_CHANNELS_MONO) {
        processMono(data, length);
    } else {
        processLeftSide(data, length);
        processRightSide(data, length);
    }
}

short IIREqualizer::limitToShort(double value) {
    if (value > SHRT_MAX) {
        return SHRT_MAX;
    } else if (value < SHRT_MIN) {
        return SHRT_MIN;
    } else {
        return (short) value;
    }
}

void IIREqualizer::setBandDbValue(int side, int index, double level) {
    if (side == EQ_SIDE_LEFT) {
        controlsLeft->setBandDbValue(index, level);
    } else {
        controlsRight->setBandDbValue(index, level);
    }
}

int IIREqualizer::getSampleRate() const {
    return sampleRate;
}

void IIREqualizer::setSampleRate(int sampleRate) {
    IIREqualizer::sampleRate = sampleRate;
}

int IIREqualizer::getChannelCount() const {
    return channelCount;
}

void IIREqualizer::setChannelCount(int channelCount) {
    IIREqualizer::channelCount = channelCount;
}

int IIREqualizer::getBands() const {
    return bands;
}

void IIREqualizer::setBands(int bands) {
    IIREqualizer::bands = bands;
}

bool IIREqualizer::isEnabled() const {
    return enabled;
}

void IIREqualizer::setEnabled(bool enabled) {
    IIREqualizer::enabled = enabled;
}
