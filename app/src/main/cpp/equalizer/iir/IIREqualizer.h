//
// Created by Roman Samoilenko on 3/4/19.
//

#ifndef CHKIN_HEARING_IIREQUALIZER_H
#define CHKIN_HEARING_IIREQUALIZER_H

#include <cstdint>
#include "XYData.h"
#include "IIRControls.h"
#include "IIRCoefficients.h"

class IIREqualizer {
public:
    static const int EQ_CHANNELS_MONO = 1;
    static const int EQ_CHANNELS_STEREO = 2;
    static const int EQ_10_BANDS = 10;

    static const int EQ_SIDE_LEFT = 0;
    static const int EQ_SIDE_RIGHT = 1;

    static const int EQ_44100_RATE = 44100;
    static const int EQ_48000_RATE = 48000;

    IIREqualizer(int bands, int sampleRate, int channelCount);

    void process(int16_t data[], int length);

    void setBandDbValue(int side, int index, double level);

    int getSampleRate() const;

    void setSampleRate(int sampleRate);

    int getChannelCount() const;

    void setChannelCount(int channelCount);

    int getBands() const;

    void setBands(int bands);

    bool isEnabled() const;

    void setEnabled(bool enabled);

private:
    int iLeft, jLeft, kLeft;
    int iRight, jRight, kRight;
    int sampleRate, channelCount, bands;
    bool enabled;

    XYData *dataHistoryLeft[EQ_10_BANDS], *dataHistoryRight[EQ_10_BANDS];

    IIRControls *controlsLeft, *controlsRight;

    void init();

    void processMono(int16_t data[], int length);

    void processLeftSide(int16_t data[], int length);

    void processRightSide(int16_t data[], int length);

    short limitToShort(double value);
};

#endif //CHKIN_HEARING_IIREQUALIZER_H
