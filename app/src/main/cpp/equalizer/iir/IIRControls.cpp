//
// Created by Roman Samoilenko on 4/4/19.
//

#include "IIRControls.h"

IIRControls::IIRControls(int size) : size(size) {
    bands = new double[size];
    for (int i = 0; i < size; i++) {
        bands[i] = 0.f;
    }
}

IIRControls::~IIRControls() {
    delete[](bands);
}

void IIRControls::setBandDbValue(int band, double value) {
    bands[band] = 2.5220207857061455181125E-01 *
                  exp(8.0178361802353992349168E-02 * value)
                  - 2.5220207852836562523180E-01;
}

int IIRControls::getSize() const {
    return size;
}

double *IIRControls::getBands() const {
    return bands;
}