//
// Created by Roman Samoilenko on 3/4/19.
//

#ifndef CHKIN_HEARING_XYDATA_H
#define CHKIN_HEARING_XYDATA_H

class XYData {
public:
    double x[3] = {0, 0, 0};
    double y[3] = {0, 0, 0};

    XYData();
};

#endif //CHKIN_HEARING_XYDATA_H
