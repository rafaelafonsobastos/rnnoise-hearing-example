//
// Created by Roman Samoilenko on 4/4/19.
//

#include "XYData.h"

XYData::XYData() {
    for (int i = 0; i < 3; i++) {
        x[i] = 0;
        y[i] = 0;
    }
}
