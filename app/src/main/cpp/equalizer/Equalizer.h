//
// Created by Roman Samoilenko on 4/4/19.
//

#ifndef CHKIN_HEARING_EQUALIZER_H
#define CHKIN_HEARING_EQUALIZER_H

#include <jni.h>
#include "iir/IIREqualizer.h"

extern IIREqualizer *equalizer;

#endif //CHKIN_HEARING_EQUALIZER_H
