//
// Created by Roman Samoilenko on 4/4/19.
//
#include "Equalizer.h"

IIREqualizer *equalizer = nullptr;

extern "C" {

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_equalizer_EqualizerHelper_create(JNIEnv *env,
                                                                             jobject,
                                                                             jint size,
                                                                             jint sampleRate,
                                                                             jint channelCount,
                                                                             jboolean enabled,
                                                                             jdoubleArray bandLevelsLeft,
                                                                             jdoubleArray bandLevelsRight) {
        equalizer = new IIREqualizer(size, sampleRate, channelCount);
        equalizer->setEnabled(enabled);
        jdouble *bandsLeft = env->GetDoubleArrayElements(bandLevelsLeft, nullptr);
        jdouble *bandsRight = env->GetDoubleArrayElements(bandLevelsRight, nullptr);

        for (int i = 0; i < size; i++) {
            equalizer->setBandDbValue(0, i, bandsLeft[i]);
            equalizer->setBandDbValue(1, i, bandsRight[i]);
        }

        env->ReleaseDoubleArrayElements(bandLevelsLeft, bandsLeft, 0);
        env->ReleaseDoubleArrayElements(bandLevelsRight, bandsRight, 0);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_equalizer_EqualizerHelper_setEnabled(JNIEnv *env,
                                                                                 jobject,
                                                                                 jboolean enabled) {
        equalizer->setEnabled(enabled);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_equalizer_EqualizerHelper_setBandDbValue(JNIEnv *env,
                                                                                     jobject,
                                                                                     jint side,
                                                                                     jint band,
                                                                                     jdouble level) {
        equalizer->setBandDbValue(side, band, level);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_equalizer_EqualizerHelper_setSampleRate(JNIEnv *env,
                                                                                    jobject,
                                                                                    jint sampleRate) {
        equalizer->setSampleRate(sampleRate);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_equalizer_EqualizerHelper_setChannelCount(JNIEnv *env,
                                                                                      jobject,
                                                                                      jint channelCount) {
        equalizer->setChannelCount(channelCount);
    }

    JNIEXPORT void JNICALL
    Java_com_chkin_hearing_data_local_audio_equalizer_EqualizerHelper_process(JNIEnv *env,
                                                                              jobject,
                                                                              jshortArray data,
                                                                              jint size) {
        jshort *array = env->GetShortArrayElements(data, nullptr);
        equalizer->process(array, size);
        env->ReleaseShortArrayElements(data, array, 0);
    }

}

